class MannequinSizesController < ApplicationController
  before_filter :set_mannequin_size, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @mannequin_sizes = MannequinSize.all
    respond_with(@mannequin_sizes)
  end

  def show
    respond_with(@mannequin_size)
  end

  def new
    @mannequin_size = MannequinSize.new
    respond_with(@mannequin_size)
  end

  def edit
  end

  def create
    @mannequin_size = MannequinSize.new(params[:mannequin_size])
    flash[:notice] = 'MannequinSize was successfully created.' if @mannequin_size.save
    respond_with(@mannequin_size)
  end

  def update
    flash[:notice] = 'MannequinSize was successfully updated.' if @mannequin_size.update_attributes(params[:mannequin_size])
    respond_with(@mannequin_size)
  end

  def destroy
    @mannequin_size.destroy
    respond_with(@mannequin_size)
  end

  private
    def set_mannequin_size
      @mannequin_size = MannequinSize.find(params[:id])
    end
end
