class Portfolio
  include Mongoid::Document
  include Mongoid::Paperclip
  field :name, type: String
  field :link, type: String

  has_mongoid_attached_file :picture,
	  :storage        => :s3,
	  :bucket_name    => 'Mobile2you',
	  :bucket    => 'Mobile2you',
	  :path           => ':attachment/:id/:style.:extension',
	  :s3_credentials => File.join(Rails.root, 'config', 's3.yml')
end
