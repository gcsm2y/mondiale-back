class StatusUserJob
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :complete, type: Boolean
  field :status_id, type: String
  field :status_title, type: String

  belongs_to :job
  belongs_to :user_app
  # belongs_to :status
end
