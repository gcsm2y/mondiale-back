class UserAppsController < ApplicationController
  before_filter :set_user_app, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    @user_apps = UserApp.all
    respond_with(@user_apps)
  end

  def show
    respond_with(@user_app)
  end

  def new
    @user_app = UserApp.new
    respond_with(@user_app)
  end

  def edit
  end

  def approve
    @user_app = UserApp.find(params[:id])
    @user_app.acceptProfile = 1
    @user_app.save(validate:false)

    ContactMailer.user_aprove(@user_app).deliver

    redirect_to user_app_path(@user_app)
  end

  def repprove
    @user_app = UserApp.find(params[:id])
    @user_app.acceptProfile = 2
    @user_app.save(validate:false)

    ContactMailer.user_reprove(@user_app).deliver

    redirect_to user_app_path(@user_app)
  end

  def create
    #if UserApp.all.count < Config.all[0].qtdSubscribe
      @user_app = UserApp.new(params[:user_app])
      flash[:notice_success] = 'Usuário criado com sucesso.' if @user_app.save
      respond_with(@user_app)
    #else
      #flash[:notice] = 'Quantidade máxima de inscritos atingida.'
      #redirect_to "/"
    #end
  end

  def update
    flash[:notice_success] = 'Usuário atualizado com sucesso.' if @user_app.update_attributes(params[:user_app])
    respond_with(@user_app)
  end

  def destroy
    @user_app.destroy
    respond_with(@user_app)
  end

  private
    def set_user_app
      @user_app = UserApp.find(params[:id])
    end
end
