class PerformanceContentsController < ApplicationController
  before_filter :set_performance_content, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    @performance_contents = PerformanceContent.all
    respond_with(@performance_contents)
  end

  def show
    respond_with(@performance_content)
  end

  def new
    @performance_content = PerformanceContent.new
    respond_with(@performance_content)
  end

  def edit
  end

  def create
    @performance_content = PerformanceContent.new(params[:performance_content])
    @performance_content.parent_name = @performance_content.performance.name
    flash[:notice_success] = 'Criado com sucesso.' if @performance_content.save
    respond_with(@performance_content)
  end

  def update
    @performance_content.update_attributes(params[:performance_content])
    @performance_content.parent_name = @performance_content.performance.name

    flash[:notice_success] = 'Atualizado com sucesso.' if @performance_content.save
    respond_with(@performance_content)
  end

  def destroy
    @performance_content.destroy
    respond_with(@performance_content)
  end

  private
    def set_performance_content
      @performance_content = PerformanceContent.find(params[:id])
    end
end
