class ContactMailer < ActionMailer::Base
	# app/view/contact_mailer
	def user_link(prodEmail, users)
		email = "adriana@agenciamondiale.com.br"
		@users = users
		headers 'X-Special-Domain-Specific-Header' => "SecretValue",
        		'from' => "#{email}",
        		'sender' => "#{email}"

		mail(from: "Equipe Mondiale Casting <#{email}>", 
		 	 to: prodEmail,
		 	 subject: 'Lista de candidatos para ' + users[0].job.title)
	end

	def user_subscribe(user)
		email = "adriana@agenciamondiale.com.br"
		# c = Config.all.first
		@user = user
		headers 'X-Special-Domain-Specific-Header' => "SecretValue",
        		'from' => "#{email}",
        		'sender' => "#{email}"

		mail(from: "Equipe Mondiale Casting <#{email}>", 
		 	 to: "adriana@agenciamondiale.com.br",
		 	 subject: 'Cadastro Mondiale')
	end

	def user_aprove(user)
		email = "adriana@agenciamondiale.com.br"
		@user = user
		headers 'X-Special-Domain-Specific-Header' => "SecretValue",
        		'from' => "#{email}",
        		'sender' => "#{email}"

		mail(from: "Equipe Mondiale Casting <#{email}>", 
		 	 to: @user.email,
		 	 subject: 'Aprovação do seu cadastro no Mondiale')
	end

	def user_reprove(user)
		email = "adriana@agenciamondiale.com.br"
		@user = user
		headers 'X-Special-Domain-Specific-Header' => "SecretValue",
        		'from' => "#{email}",
        		'sender' => "#{email}"

		mail(from: "Equipe Mondiale Casting <#{email}>",
		 	 to: @user.email,
		 	 subject: 'Cadastro reprovado no Mondiale')
	end
end