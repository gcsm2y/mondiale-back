class UserProducersController < ApplicationController
  before_filter :set_user_producer, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser, except: [:edit,:show]

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    @user_producers = UserProducer.all
    respond_with(@user_producers)
  end

  def show
    respond_with(@user_producer)
  end

  def new
    @user_producer = UserProducer.new
    respond_with(@user_producer)
  end

  def edit
  end

  def create
    @user_producer = UserProducer.new(params[:user_producer])
    flash[:notice_success] = 'Produtor criado com sucesso.' if @user_producer.save
    respond_with(@user_producer)
  end

  def update
    flash[:notice_success] = 'Produtor atualizado com sucesso.' if @user_producer.update_attributes(params[:user_producer])
    respond_with(@user_producer)
  end

  def destroy
    @user_producer.destroy
    respond_with(@user_producer)
  end

  private
    def set_user_producer
      @user_producer = UserProducer.find(params[:id])
    end
end
