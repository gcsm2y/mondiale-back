class HomeController < ApplicationController

	def index
		if current_user.class != SuperAdmin
			redirect_to "/user_applicants"
		end

		@data  = Date.today
		@dataFim = Date.today

 		if params[:data_inicio]
			@data = params[:data_inicio].to_date
		end

		if @data.nil?
			@data = Date.today 
		end

		if params[:data_fim]
			@dataFim = params[:data_fim].to_date 
		end

		if @dataFim.nil?
			@dataFim = Date.today
		end


		if @data == @dataFim
			@dataFim = @dataFim + 1.days
		end
		@data = @data + 3.hours

		@user = nil
		@store = nil
		tryMateria = false
		tryProfessor = false

		if !params[:store].nil? && params[:store].length > 1
			tryProfessor = true
 		   	 @store = Store.find(params[:store])
		end

		@orders = []
		if !@store.nil?
			@orders = @store.orders.where( :paidToStore => false)
		elsif tryProfessor && @store.nil?
			# @orders = Order.where(:created_at => nil, :user_id.ne => nil)
		elsif tryMateria
			# @orders = Order.where(:created_at.gte => @data, :created_at.lte => @dataFim, :user => @user, :user_id.ne => nil)
		else
			# @orders = Order.where(:created_at.gte => @data, :created_at.lte => @dataFim, :user_id.ne => nil)
		end

 		if @store.nil?
			@store = ""
		elsif !params[:store].nil? 
			@store = params[:store]
		else
			@store = @store.name
		end

		if @user.nil?
			@user = ""
		end
		@orders = []
 	end

  def download
	@data  = Date.today
	@dataFim = Date.today

	if params[:data_inicio]
		@data = params[:data_inicio].to_date
	end

	if @data.nil?
		@data = Date.today 
	end

	if params[:data_fim]
		@dataFim = params[:data_fim].to_date 
	end

	if @dataFim.nil?
		@dataFim = Date.today
	end


	if @data == @dataFim
		@dataFim = @dataFim + 1.days
	end
	@data = @data + 3.hours

	@user = nil
	@store = nil
	tryMateria = false
	tryProfessor = false

	if !params[:store].nil? && params[:store].length > 1
		tryProfessor = true
		   	 @store = Store.find(params[:store])
	end

	@orders = []
	if !@store.nil?
		@orders = @store.orders.where( :paidToStore => false)
	elsif tryProfessor && @store.nil?
		@orders = Order.where(:created_at => nil, :user_id.ne => nil)
	elsif tryMateria
		@orders = Order.where(:created_at.gte => @data, :created_at.lte => @dataFim, :user => @user, :user_id.ne => nil)
	else
		@orders = Order.where(:created_at.gte => @data, :created_at.lte => @dataFim, :user_id.ne => nil)
	end

	if @store.nil?
		@store = ""
	elsif !params[:store].nil? 
		@store = params[:store]
	else
		@store = @store.name
	end

	if @user.nil?
		@user = ""
	end
    alunos = "ID,Data,Status,Prestador,Loja,Cliente,Serviço,Valor,Valor Mobile2you\n"
    @orders.each do |aluno|
      alunos +=  aluno.id.to_s +  "," 
      alunos += aluno.date.strftime("%d/%m/%Y %H:%M") +  "," 
      alunos += aluno.status +  "," 
      alunos += aluno.store.provider.name + "," 
      alunos += aluno.store.name + "," 
      alunos += aluno.user.name + "," 
      alunos += aluno.service.name + "," 
      alunos += (aluno.amount/100).to_s + "," 
      alunos += ((aluno.service.transfer_percent/100) *  aluno.amount/100).to_s + "," 
      alunos += "\n"
    end

    send_data alunos, :filename => 'pedidos.csv' 

  end

  def gerar_boleto
  	o = Order.find(params[:order])
	transaction = PagarMe::Transaction.new(
	amount:         o.amount,    # in cents
    payment_method: 'boleto'
	  )
  	transaction.charge
  	redirect_to transaction.boleto_url     # => boleto's URL
  end


end