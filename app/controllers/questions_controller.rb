class QuestionsController < ApplicationController
  before_filter :set_question, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @questions = Question.all
    respond_with(@questions)
  end

  def show
    respond_with(@question)
  end

  def new
    @question = Question.new
    respond_with(@question)
  end

  def edit
  end

  def create
    @question = Question.new(params[:question])
    flash[:notice_success] = 'Pergunta criada com sucesso.' if @question.save
    respond_with(@question)
  end

  def update
    flash[:notice_success] = 'Pergunta atualizada com sucesso.' if @question.update_attributes(params[:question])
    respond_with(@question)
  end

  def destroy
    @question.destroy
    respond_with(@question)
  end

  private
    def set_question
      @question = Question.find(params[:id])
    end
end
