class UsersController < ApplicationController
  before_filter :set_user, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser,except: [:show, :edit]

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    redirect_to "/admin"
    # @users = User.all #.where(:_type.ne => "SuperAdmin")

    # respond_to do |format|
    #   format.html # index.html.erb
    #   format.json { render json: @categories }
    # end
  end

  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
        format.html { redirect_success("Usuário removido com sucesso!",:users, :index)}
      format.json { head :no_content }
    end
  end

  def new
    @user = User.new
    respond_with(@user)
  end

  private
    def set_user
      @user = User.find(params[:id])
    end
end
