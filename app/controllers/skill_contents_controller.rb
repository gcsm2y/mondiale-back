class SkillContentsController < ApplicationController
  before_filter :set_skill_content, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    @skill_contents = SkillContent.all
    respond_with(@skill_contents)
  end

  def show
    respond_with(@skill_content)
  end

  def new
    @skill_content = SkillContent.new
    respond_with(@skill_content)
  end

  def edit
  end

  def create
    @skill_content = SkillContent.new(params[:skill_content])
    @skill_content.parent_name = @skill_content.skill.name

    flash[:notice_success] = 'Criado com sucesso.' if @skill_content.save
    respond_with(@skill_content)
  end

  def update
    @skill_content.update_attributes(params[:skill_content])
    @skill_content.parent_name = @skill_content.skill.name
    
    flash[:notice_success] = 'Atualizado com sucesso.' if @skill_content.save
    respond_with(@skill_content)
  end

  def destroy
    @skill_content.destroy
    respond_with(@skill_content)
  end

  private
    def set_skill_content
      @skill_content = SkillContent.find(params[:id])
    end
end
