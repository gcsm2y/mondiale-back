class Appearance
  include Mongoid::Document
  field :name, type: String

  has_many :appearance_contents, :dependent => :destroy
end
