class PortfoliosController < ApplicationController
  before_filter :set_portfolio, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @portfolios = Portfolio.all
    respond_with(@portfolios)
  end

  def show
    respond_with(@portfolio)
  end

  def new
    @portfolio = Portfolio.new
    respond_with(@portfolio)
  end

  def edit
  end

  def create
    @portfolio = Portfolio.new(params[:portfolio])
    flash[:notice_success] = 'Criado com sucesso.' if @portfolio.save
    respond_with(@portfolio)
  end

  def update
    flash[:notice_success] = 'Atualizado com sucesso.' if @portfolio.update_attributes(params[:portfolio])
    respond_with(@portfolio)
  end

  def destroy
    @portfolio.destroy
    respond_with(@portfolio)
  end

  private
    def set_portfolio
      @portfolio = Portfolio.find(params[:id])
    end
end
