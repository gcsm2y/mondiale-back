class SkillContent
  include Mongoid::Document
  field :name, type: String
  field :parent_name, type: String

  belongs_to :skill
  
  has_and_belongs_to_many :users_prof, :class_name => "UserApp", :inverse_of => :skill_contents_prof
  has_and_belongs_to_many :users_jr, :class_name => "UserApp", :inverse_of => :skill_contents_jr

  has_and_belongs_to_many :job_skill_prof, :class_name => "Job", :inverse_of => :skill_job_contents_prof
  has_and_belongs_to_many :job_skill_jr, :class_name => "Job", :inverse_of => :skill_job_contents_jr

end
