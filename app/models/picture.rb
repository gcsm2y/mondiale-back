class Picture
  include Mongoid::Document
  include Mongoid::Paperclip 
  # require 'paperclip_processors/watermark' 

  # acts_as_paranoid

  has_mongoid_attached_file :file,
      :storage        => :s3,
      :bucket_name    => 'Mobile2you',
      :bucket    => 'Mobile2you',
      :styles => {  
                    :small => "50x50>" 
                    # :original => {  
                    #                 :processors => [:watermark],
                    #                 :geometry => '800>', 
                    #                 :watermark_path => "/img/LogoHorizontal.png" }
                  },
      # :styles => { :small => "50x50>", medium: '300x300>', thumb: '100x100>' },
      # :processors => [:thumbnail, :compression],
      :path           => ':attachment/:id/:style.:extension',
      :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

   # belongs_to :answer
   belongs_to :user_app
   belongs_to :user_applicant
end