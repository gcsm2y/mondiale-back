class PerformanceContent
  include Mongoid::Document
  field :name, type: String
  field :parent_name, type: String

  belongs_to :performance
  has_and_belongs_to_many :user_apps
  has_and_belongs_to_many :jobs

  # has_and_belongs_to_many :user_prof, :class_name => "UserApp", :inverse_of => :performance_contents_prof
  # has_and_belongs_to_many :user_jr, :class_name => "UserApp", :inverse_of => :performance_contents_jr
  
  # has_and_belongs_to_many :job_performance_prof, :class_name => "Job", :inverse_of => :performance_contents_prof
  # has_and_belongs_to_many :job_performance_jr, :class_name => "Job", :inverse_of => :performance_contents_jr

end
