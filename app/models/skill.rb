class Skill
  include Mongoid::Document
  field :name, type: String

  has_many :skill_contents, :dependent => :destroy

  
  #account_controller >skill
  def self.mapSkill (array, c_user)
    array.map{ |s|{ 
      :id => s.id,
      :name => s.name, 
      :options => mapSkillContent(s.skill_contents, c_user)
    }}
  end

  def self.mapSkillContent (array, c_user)
    array.map{ |s|{ 
      :id => s.id,
      :name => s.name,
      :status => s.users_jr.include?(c_user) ? 1 : (s.users_prof.include?(c_user) ? 2 : 0)
    }}
  end
end
