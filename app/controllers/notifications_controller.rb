class NotificationsController < ApplicationController
  before_filter :set_notification, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @notifications = Notification.all
    respond_with(@notifications)
  end

  def show
    respond_with(@notification)
  end

  def new
    @notification = Notification.new
    respond_with(@notification)
  end

  def edit
  end

  def create
    @notification = Notification.new(params[:notification])
    flash[:notice_success] = 'Notificação criada com sucesso.' if @notification.save
    respond_with(@notification)
  end

  def update
    flash[:notice_success] = 'Notificação atualizada com sucesso.' if @notification.update_attributes(params[:notification])
    respond_with(@notification)
  end

  def destroy
    @notification.destroy
    respond_with(@notification)
  end

  private
    def set_notification
      @notification = Notification.find(params[:id])
    end
end
