class Answer
  include Mongoid::Document
  include Mongoid::Paperclip
  include Mongoid::Timestamps
  field :content, type: String

  has_mongoid_attached_file :file,
      :storage        => :s3,
      :bucket_name    => 'Mobile2you',
      :bucket    => 'Mobile2you',
      :styles => {  :small => "130x130>"},
      :path           => ':attachment/:id/:style.:extension',
      :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

  belongs_to :user_app
  belongs_to :user_applicant
  belongs_to :question
end
