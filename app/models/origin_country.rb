class OriginCountry
  include Mongoid::Document
  field :name, type: String
  has_many :user_apps
end
