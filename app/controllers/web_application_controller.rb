class WebApplicationController < ActionController::Base

  before_filter :authenticate_user!, :except => [:open_job_subscribe, :public_perfil, :download]

  def index
  end

  def redirect_success(message, controller, action)
  	redirect_to({:controller=> controller, :action => action}, :flash => { :notice_success => message })   
  end

  def redirect_success_home(message)
    redirect_to('/home', flash: { notice_success: message })
  end

  def redirect_success_show(message, controller, id)
    redirect_to({:controller=> controller, :action => :show, :id => id}, :flash => { :notice_success => message })   
  end

  def redirect_success_index_show(message, controller, id)
    redirect_to({:controller=> controller, :action => :index, :store => id}, :flash => { :notice_success => message })   
  end

  def redirect_success_index_show2(message, controller, id)
    redirect_to({:controller=> controller, :action => :index, :especialidade => id}, :flash => { :notice_success => message })   
  end


  def redirect_error(message, controller, action)
  	redirect_to({:controller=> controller, :action => action}, :flash => { :notice_error => message })   
  end

end
