class Web::JobsController < ApplicationController

  
  api :POST, '/jobs/new'
  def new
		@job = Job.new(
   		:jobType => params[:jobType],
   		:client => params[:client],
   		:studio => params[:studio],
   		:product => params[:product],
   		:placement => params[:placement],
   		:territory => params[:territory],
   		:midia => params[:midia],
   		:exclusivity => params[:exclusivity],
   		:duration => params[:duration],
   		:qtdDaily => params[:qtdDaily],
   		:desc => params[:desc],
      :cacheType => params[:cacheType],
      :cache => params[:cache],
   		:cacheTotal => params[:cacheTotal],
   		:presential => params[:presencial],
   		:aprovationType => params[:aprovationType],
   		:aceitaFiguracao => params[:aceitaFiguracao]
   	)

    # performance
    @job.performance_contents_ids = params[:performances]

		# skills
    @job.skill_contents_prof_ids << params[:dancaprofissional]
    @job.skill_contents_prof_ids << params[:circoprofissional]
    @job.skill_contents_prof_ids << params[:dirigeprofissional]
    @job.skill_contents_prof_ids << params[:esporteprofissional]
    @job.skill_contents_prof_ids << params[:musicaprofissional]
    @job.skill_contents_prof_ids << params[:outrasprofissional]

    @job.skill_contents_jr_ids << params[:dancaamadoras]
    @job.skill_contents_jr_ids << params[:circoamadoras]
    @job.skill_contents_jr_ids << params[:dirigeamadoras]
    @job.skill_contents_jr_ids << params[:esporteamadoras]
    @job.skill_contents_jr_ids << params[:musicaamadoras]
		@job.skill_contents_jr_ids << params[:outrasamadoras]

	   # Appearance
		@job.appearance_content_ids << params[:corOlhos]
		@job.appearance_content_ids << params[:corCabelos]
		@job.appearance_content_ids << params[:tipoFisico]
		@job.appearance_content_ids << params[:tipoCabelo]
		@job.appearance_content_ids << params[:idioma]
		@job.appearance_content_ids << params[:sotaque]
		@job.appearance_content_ids << params[:expGenero]

		@job.user_producer = UserProducer.all.first #current_user

		if @job.save(validate:false)
         redirect_to "/search?job=" + @job.id
      	# render json: { :message => search, :ids => search_ids, :users => @users}, :status => 200
    else
   		render json: { :message => 'Não foi possivel criar a Job' }, :status => 500  
    end
  end

  api :POST, '/jobs/newOpenJob'
  def newOpenJob
      @job = Job.new(
         :title => params[:title],
         :perfil => params[:perfil],
         # :client => params[:client],
         :desc => params[:desc],
         :local => params[:local],
         :rules => params[:rules],
         :cache => params[:cacheTotal],
         :cacheTotal => params[:cacheTotal],
         :byWeb => true
      )
      
      @job.user_producer = current_user

      @job.save(validate:false)

      #Solicitar: 0 - texto, 1 - link, 2 - imagem, 3 - video
      i = 0
      if !params[:perguntaType][0].blank?
         while i < params[:perguntaType].count  do
            if !params[:perguntaType][i].blank?
               @job.questions.create(:title => params[:perguntaType][i].to_s, :type => 0)
            end
            i +=1
         end
      end
      i = 0
      if !params[:linkType][0].blank?
         while i < params[:linkType].count  do
            if !params[:linkType][i].blank?
               @job.questions.create(:title => params[:linkType][i].to_s, :type => 1)
            end
            i +=1
         end
      end
      i = 0
      if !params["fotoType"][0].blank?
         while i < params["fotoType"].count  do
            if !params["fotoType"][i].blank?
               @job.questions.create(:title => params["fotoType"][i].to_s, :type => 2)
            end
            i +=1
         end
      end
      i = 0
      if !params[:videoType][0].blank?
         while i < params[:videoType].count  do
            if !params[:videoType][i].blank?
               @job.questions.create(:title => params[:videoType][i].to_s, :type => 3)
            end
            i +=1
         end
      end

      # @job.user_producer = UserProducer.all.first #current_user

      if @job.save(validate:false)
         render json: { :Job => @job}, :status => 200
      else
         render json: { :message => 'Não foi possivel criar a Job' }, :status => 500  
      end
      # render json: { :Job => "Cadastro realizado com sucesso"} , :status => 200
  end

  api :POST, '/jobs/editOpenJob'
  def editOpenJob
    job  = Job.find(params[:id])
    job.title = params[:title]
    job.perfil = params[:perfil]
    job.desc = params[:desc]
    job.local = params[:local]
    job.rules = params[:rules]
    job.cache = params[:cacheTotal]
    job.cacheTotal = params[:cacheTotal]

    #Solicitar: 0 - texto, 1 - link, 2 - imagem, 3 - video
    i = 0
    if !params[:perguntaType][0].blank?
       while i < params[:perguntaType].count  do
          if !params[:perguntaType][i].blank?
             job.questions.create(:title => params[:perguntaType][i].to_s, :type => 0)
          end
          i +=1
       end
    end

    i = 0
    if !params[:linkType][0].blank?
       while i < params[:linkType].count  do
          if !params[:linkType][i].blank?
             job.questions.create(:title => params[:linkType][i].to_s, :type => 1)
          end
          i +=1
       end
    end
    i = 0
    if !params["fotoType"][0].blank?
       while i < params["fotoType"].count  do
          if !params["fotoType"][i].blank?
             job.questions.create(:title => params["fotoType"][i].to_s, :type => 2)
          end
          i +=1
       end
    end
    
    i = 0
    if !params[:videoType][0].blank?
       while i < params[:videoType].count  do
          if !params[:videoType][i].blank?
             job.questions.create(:title => params[:videoType][i].to_s, :type => 3)
          end
          i +=1
       end
    end


    if params[:perguntaTypeS]
      params[:perguntaTypeS].keys.each do |key|
        q = Question.find(key.to_s)
        q.title = params[:perguntaTypeS][key]
        q.save
      end
    end

    if job.save(validate:false)
       # redirect_to "/user_applicants/" + job.id
       render json: { :message => 'Ediçao da Job com sucesso' }, :status => 200
    else
       render json: { :message => 'Não foi possivel editar a Job' }, :status => 500  
    end
  end

  api :POST, '/jobs/applyOpenJob'
  def applyOpenJob
      j = Job.find(params[:job_id])
      user = UserApplicant.new(
           :name => params[:name],
           :gender => params[:gender],
           :birthday => params[:birthday],
           :parent_name => params[:parent_name],
           :parent_cpf => params[:parent_cpf],
           :country => params[:country],
           :state => params[:state],
           :city => params[:city],
           :phone => params[:phone],
           :desc => params[:desc],
           :facebook => params[:facebook],
           :instagram => params[:instagram],
           :cacheMin => params[:cacheMin],
           :email => params[:email]
      )
      user.job = j
      
      if user.save(validate:false)
         if params[:perguntaResposta]
            params[:perguntaResposta].keys.each do |key|
               q = Question.find(key.to_s)
               if q.type == 0
                  q.answers.create(:user_applicant => user, :content => params[:perguntaResposta][key])
               end
            end
         end
         if params[:linkResposta]
            params[:linkResposta].keys.each do |key|
               q = Question.find(key.to_s)
               if q.type == 1
                  q.answers.create(:user_applicant => user, :content => params[:linkResposta][key])
               end
            end
         end

          if !params[:pictures].blank?
            firstPhoto = true
            params[:pictures].keys.each do |key|
              q = Question.find(key.to_s)
              params[:pictures][key].each { |image|
                if firstPhoto
                  user.pictures.create(file: image)
                  firstPhoto = false
                end
                q.answers.create(:user_applicant => user, file: image)
              }
            end
          end

         if !params[:videos].blank?
            params[:videos].each do |vid|
               user.videos.create(:file => vid)
            end
         end
         render json: { :message => 'Candidato adicionado na Job', :user => user}, :status => 200
      else
         render json: { :message => 'Não foi possivel adicionar Candidato' }, :status => 500  
      end
  end

   api :POST, '/jobs/editApplyOpenJob'
  def editApplyOpenJob
      user = UserApplicant.find(params[:user_id])
      user.name = params[:name]
      user.gender = params[:gender]
      user.birthday = params[:birthday]
      user.parent_name = params[:parent_name]
      user.parent_cpf = params[:parent_cpf]
      user.country = params[:country]
      user.state = params[:state]
      user.city = params[:city]
      user.phone = params[:phone]
      user.desc = params[:desc]
      user.cacheMin = params[:cacheMin]
      user.facebook = params[:facebook]
      user.instagram = params[:instagram]
      user.email = params[:email]

      if user.save(validate:false)
         # if params[:perguntaResposta]
         #    params[:perguntaResposta].keys.each do |key|
         #       q = Question.find(key.to_s)
         #       if q.type == 0
         #          q.answers.create(:user_applicant => user, :content => params[:perguntaResposta][key])
         #       end
         #    end
         # end
         # if params[:linkResposta]
         #    params[:linkResposta].keys.each do |key|
         #       q = Question.find(key.to_s)
         #       if q.type == 1
         #          q.answers.create(:user_applicant => user, :content => params[:linkResposta][key])
         #       end
         #    end
         # end

          if !params[:pictures].blank?
            params[:pictures].keys.each do |key|
              q = Question.find(key.to_s)
              params[:pictures][key].each { |image|
                if user.pictures.count == 0
                  user.pictures.create(file: image)
                end
                q.answers.create(:user_applicant => user, file: image)
              }
            end
          end

         if !params[:videos].blank?
            user.videos.destroy_all
            params[:videos].each do |vid|
               user.videos.create(:file => vid)
            end
         end
         render json: { :message => 'Candidato atualizado na Job', :user => user}, :status => 200
      else
         render json: { :message => 'Não foi possivel atualizar Candidato' }, :status => 500  
      end
  end

  api :POST, '/jobs/apply'
  def apply
    user = User.find(params[:userId])
    job = Job.find(params[:jobId])

    if !user.job_ids.include?(job.id)
       user.job_ids << job.id
    else
       user.job_ids.delete(job.id)
    end
    
    if user.save(validate:false)
       render json: { :message => 'Adicionado/removido Job', :userId => params[:userId], :jobId => params[:jobId]}, :status => 200
    else
       render json: { :message => 'Não foi possivel adicionar Job' }, :status => 500  
    end
  end
end