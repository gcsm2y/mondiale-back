class StatusUserJobsController < ApplicationController
  before_filter :set_status_user_job, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @status_user_jobs = StatusUserJob.all
    respond_with(@status_user_jobs)
  end

  def show
    respond_with(@status_user_job)
  end

  def new
    @status_user_job = StatusUserJob.new
    respond_with(@status_user_job)
  end

  def edit
  end

  def create
    @status_user_job = StatusUserJob.new(params[:status_user_job])
    flash[:notice] = 'StatusUserJob was successfully created.' if @status_user_job.save
    respond_with(@status_user_job)
  end

  def update
    flash[:notice] = 'StatusUserJob was successfully updated.' if @status_user_job.update_attributes(params[:status_user_job])
    respond_with(@status_user_job)
  end

  def destroy
    @status_user_job.destroy
    respond_with(@status_user_job)
  end

  private
    def set_status_user_job
      @status_user_job = StatusUserJob.find(params[:id])
    end
end
