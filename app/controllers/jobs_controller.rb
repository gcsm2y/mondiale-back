class JobsController < ApplicationController
  before_filter :set_job, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !(current_user.isSuperAdmin? || current_user.isUserProducer?)
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    @jobs = Job.all
    respond_with(@jobs)
  end

  def show
    respond_with(@job)
  end

  def new
    @job = Job.new
    respond_with(@job)
  end

  def edit
  end

  def create
    @job = Job.new(params[:job])
    flash[:notice_success] = 'Vaga criada com sucesso.' if @job.save
    respond_with(@job)
  end

  def update
    # request.fullpath
    if params[:perguntaType]
      params[:perguntaType].keys.each do |key|
        q = Question.find(key.to_s)
        q.title = params[:perguntaType][key]
        q.save
      end
    end
    flash[:notice_success] = 'Vaga atualizada com sucesso.' if @job.update_attributes(params[:job])
    if request.referrer.include?("user_applicants")
      redirect_to "/user_applicants/" + @job.id
    else
      respond_with(@job)
    end
  end

  def destroy
    @job.destroy
    respond_with(@job)
  end

  private
    def set_job
      @job = Job.find(params[:id])
    end
end
