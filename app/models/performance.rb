class Performance
  include Mongoid::Document
  field :name, type: String

  has_many :performance_contents, :dependent => :destroy
end
