Mobile2youServer::Application.routes.draw do

  resources :user_applicants
  resources :status_user_jobs
  resources :mannequin_sizes
  resources :answers
  resources :phones
  resources :questions
  resources :statuses
  resources :nationalities
  resources :origin_countries
  resources :schoolings
  resources :passport_nationalities
  resources :cnhs
  resources :notifications
  resources :portfolios
  resources :addresses
  resources :pictures
  resources :user_apps
  resources :super_admins
  resources :usuarios
  resources :webs
  resources :admins
  resources :skill_contents
  resources :appearance_contents
  resources :performance_contents
  resources :performances
  resources :appearances
  resources :skills
  resources :jobs
  resources :user_producers
  resources :download

  apipie
  devise_for :users
  resources :users

  # match 'pesquisa.agenciamondiale.com.br' => redirect('https://agenciamondiale.com.br/')
  # if request.fullpath.include?("pesquisa.agenciamondiale")
  #    redirect_to "https://agenciamondiale.com.br/"
  #  end

  # get '/' => 'openJob', :constraints => { :subdomain => /.+/ }
  # constraints subdomain: 'pesquisa' do
  #   root to: 'openJob'
  # end

  # constraints subdomain: false do
    # root to: 'webs#open_job'
  # end
  constraints(Subdomain) do
    match '/' => 'webs#open_job'
  end
  root to: 'home#index'

  # get '/webs/download'=> 'webs#download'
  get 'index' => 'webs#index'
  get 'signup' => 'webs#user_signup'
  get 'anunciar' => 'webs#new_job'
  get 'gerenciar' => 'webs#manage_jobs'
  get 'openJob' => 'webs#open_job'
  get 'editOpenJob/:id' => 'webs#edit_open_job'
  get 'openJobSub' => 'webs#open_job_subscribe'
  get 'search' => 'webs#search_users'
  get 'admin' => 'home#index'
  get 'login' => 'home#index'
  get "alterar_senha" => "usuarios#edit"
  get "meu_perfil" => "usuarios#edit"
  get "publicPerfil" => "webs#public_perfil"
  get "editPublicPerfil" => "webs#edit_public_perfil"
  get 'approve/:id' => 'user_apps#approve', as: :approve
  get 'repprove/:id' => 'user_apps#repprove', as: :repprove
  get 'seen/:id' => 'user_applicants#seen', as: :seen
  post 'user_applicants/destroy_many' 
  post 'user_applicants/send_email' 
  get 'get_user_perfil'  => 'webs#get_user_perfil'


  resources :user_applicants, only: [:index, :show] do
    resource :download, only: [:show]
  end

  namespace :web do
    post "users/signin"
    post 'users/new'
    post 'jobs/new'
    post 'jobs/newOpenJob'
    post 'jobs/editOpenJob'
    post 'jobs/apply'
    post 'jobs/applyOpenJob'
    post 'jobs/editApplyOpenJob'
    get 'users/download'  
  end

  namespace :webservices do
    post "login/signin"
    post "login/signinFacebook"
    post "login/signup"
    post "login/forgotPass"
    # get 'jobs/openPesquisaJobs'


    post "account/updatePassword"
    post "account/checkPassword"
    post "account/updatePhoto"
    post "account/registerToken"
    
    # get 'account/getSubscribeInfo'
    # get "account/getAbout"
    # get "account/getNotifications"

    # put "account/updateAbout"
    # put 'account/updateLanguage'

    get "jobs/openJobs"
    get "jobs/myJobs"

    post "jobs/apply"
    post "jobs/addMediaAnswer"

    delete "jobs/removeMediaAnswer"

    get "account/about"
    get "account/myGallery"
    get "account/getCharacteristics"
    get "account/getFirstContact"
    get "account/getSkills"
    get "account/getPersonalData"
    get "account/getSocialNetworks"
    get "account/notifications"
    
    post "account/createFirstContact"
    post "account/addCurriculum"
    post "account/addGallery"
    post "account/addPicture"

    put "account/editFirstContact"
    put "account/editSkills"
    put "account/editPersonalData"
    put "account/editSocialNetworks"
    put "account/editCharacteristics"

    delete "account/removeGallery"
    delete "account/removePicture"
    delete "account/removeCurriculum"
  end

  match ':controller(/:action(/:id))(.:format)'

end
