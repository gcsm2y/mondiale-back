class Address
  include Mongoid::Document
  field :zip, :type => String, :default => ""
  field :address, :type => String, :default => ""
  field :addressLocal, :type => String, :default => ""
  field :addressComp, :type => String, :default => ""
  field :addressNeighborhood, :type => String, :default => ""
  field :addressState, :type => String, :default => ""
  field :addressCity, :type => String, :default => ""

  belongs_to :user_app
end
