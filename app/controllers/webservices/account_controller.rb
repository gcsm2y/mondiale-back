class Webservices::AccountController <  WebservicesController


  api :POST, '/account/registerToken'
  formats ['json']
  param :token, String, :desc => "User Token", :required => true
  param :os, String, :desc => "Android 5.1, iOs 11 ...", :required => true
  param :device, String, :desc => "MotoG...", :required => true
  error 400, "Missing token header"
  description <<-EOS
   == Open method, for logged or new users
   == Response
     {}
   EOS
# AAAAujxSqNw:APA91bFlLHynldrznqIFKJw8o-10JLJGragcBPN2-sWrsnISDoy-KyuPUDLThO2ei7gNVRl8fidTjwcXdDtHiRYctCxPQOgWf1gTUp0gxePiMMCmSk8lhOA0dKKqa4aH2bUPSScPRMFd
  def registerToken
   current_user.token = params[:token]
   current_user.os = params[:os]
   current_user.device = params[:device]
   current_user.save(validate: false)
   render :json => {}
  end

  api :POST, '/account/updatePassword'
  formats ['json']
  param :current_password, String, :desc => "Current Password", :required => true, :missing_message => lambda { "senha atual ausente" }
  param :password, String, :desc => "New password", :required => true, :missing_message => lambda { "nova senha ausente" }
  param :password_confirmation, String, :desc => "New password confirmation", :required => true, :missing_message => lambda { "confirmação de senha ausente" }
  error 403, "parâmetros inválidos"
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"

  def updatePassword
    u = current_user
    user_id = current_user.id
    if params[:password].length < 6
      render :json => {message: "A senha deve ter 6 caracteres"}, :status => 403
    elsif params[:password] != params[:password_confirmation]
      render :json => {message: "Senha atual e confirmação diferentes"}, :status => 403
    elsif ! u.valid_password?(params[:current_password])
      render :json => {message: "Senha atual inválida"}, :status => 403
    elsif u.update_with_password(params)
      sign_in current_user, :bypass => true
      render :json =>  {:error_code => "0"}.to_json
    else
      render :nothing => true, :status => 401
    end
  end


  api :POST, '/account/checkPassword'
  formats ['json']
  param :password, String, :desc => "password", :required => true, :missing_message => lambda { "nova senha ausente" }
  error 403, "parâmetros inválidos"
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
    == Response
    {}
    EOS

  def checkPassword
    u = current_user
    user_id = current_user.id
    if u.valid_password?(params[:password])
      render :json => {}
    else
      render :nothing => true, :status => 403
    end
  end

###
  # api :POST, '/account/updatePhoto'
  # formats ['json']
  # error 401, "Usuário não autenticado"
  # error 500, "Erro desconhecido"
  # description <<-EOS
  #   == picture
  # EOS

  # def updatePhoto
  #   u = current_user
  #   u.picture = params[:picture]
  #   if !params[:photo_64].nil?
  #    tempfile = Tempfile.new("fileupload")
  #    tempfile.binmode
  #    tempfile.write(Base64.decode64(params[:photo_64]))
  #    uploaded_file = ActionDispatch::Http::UploadedFile.new(:tempfile => tempfile, :filename => "temp", :original_filename => "temp2") 
  #    u.picture =  uploaded_file
  #   end
  #   u.changedPhoto = true
  #   u.save(validate: false)
  #   render :json =>  User.mapUser(u).to_json
  # end


  # api :PUT, '/account/updateAbout'
  # formats ['json']
  # param :gender, String, :desc => "gender", :required => false, :missing_message => lambda { "telefone ausente" } 
  # param :hotelAddress, String, :desc => "address", :required => false, :missing_message => lambda { "" } 
  # param :cellphone, String, :desc => "User Telephone", :required => false, :missing_message => lambda { "telefone ausente" } 
  # param :name, String, :desc => "User Name", :required => false, :missing_message => lambda { "telefone ausente" } 
  # param :email, String, :desc => "email", :required => false, :missing_message => lambda { "telefone ausente" } 
  # param :cpf, String, :desc => "cpf Name", :required => false, :missing_message => lambda { "telefone ausente" } 
  # param :birthday, String, :desc => "birthday", :required => false, :missing_message => lambda { "telefone ausente" } 
  # param :zip, String, :desc => "zip", :required => false, :missing_message => lambda { "telefone ausente" } 

  # error 401, "Usuário não autenticado"
  # error 500, "Erro desconhecido"
  
  # description <<-EOS
  # == gender: 1 - Masculino, 2 - Feminino

  # == Response
  # {
  #   "id": "59f24cfa34fc0c324000000e",
  #   "name": "User A",
  #   "email": "usera@gmail.com",
  #   "birthday": "19/07/1998",
  #   "cellphone": "",
  #   "gender": "1",
  #   "picture": "http://s3.amazonaws.com/Mobile2you/pictures/59f24cfa34fc0c324000000e/original.png?1509051642"
  # }
  # EOS
  # def updateAbout
  #   u = UserApp.find(current_user.id)
  #   u.gender = params[:gender].nil? ? u.gender : params[:gender]
  #   u.email = params[:email].nil? ? u.email : params[:email]
  #   u.cellphone = params[:cellphone].nil? ? u.phone : params[:cellphone]
  #   u.name = params[:name].nil? ? u.name : params[:name]
  #   u.cpf = params[:cpf].nil? ? u.cpf : params[:cpf]
  #   u.zip = params[:zip].nil? ? u.zip : params[:zip]
  #   u.birthday = params[:birthday].nil? ? u.birthday : params[:birthday]

  #   u.save(validate: false)
  #   render :json => User.mapUser(u).to_json
  # end


  # api :GET, '/account/getAbout'
  # formats ['json']
  # error 401, "Usuário não autenticado"
  # error 500, "Erro desconhecido"
  # description <<-EOS
  # == Response
  # {
  #   "id": "59aeaf4e3e85a38b23000050",
  #   "name": "Caio",
  #   "email": "caoi@gmail.com",
  #   "birthday": "19/09/1990",
  #   "cellphone": "(13)3232-3223",
  #   "gender": 1,
  #   "picture": "http://"
  # }
  # EOS
  # def getAbout
  #   a = User.mapUser(UserApp.find(current_user.id))
  #   render :json => a.to_json
  # end
###

  api :GET, '/account/about'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  {
    "id": "1km4l1km4lk1231",
    "age": "23 anos",
    "place": "São Paulo",
    "flag": "https://images-na.ssl-images-amazon.com/images/I/51jjmFJcrbL._SX770_.jpg",
    "views": "12k"
  }
  EOS
  def about
    render :json => UserApp.mapAbout(UserApp.find(current_user.id))
  end
  

  api :GET, '/account/myGallery'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  [
    {
      "picture": "https://fordhamffp.files.wordpress.com/2017/11/null21.png?w=308&h=462&zoom=2",
      "video": null,
      "id": "fdkcmldk3914mklsamd102as129l"
    },
    {
      "picture": "https://fordhamffp.files.wordpress.com/2017/11/null27.png?w=289&h=434&zoom=2",
      "video": "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
      "id": "lkmdfkmsdfmsdlkmfsdlkfmalwksd"
    }
  ]
  EOS
  def myGallery
    render :json => UserApp.mapGallery(current_user)
  end


  #Add addGallery 
  api :POST, '/account/addGallery'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == gallery_picture
  == gallery_video
  EOS
  def addGallery
    user = UserApp.find(current_user.id)

    if !params[:gallery_picture].nil?
      user.pictures.create(:file => params[:gallery_picture])
    end

    if !params[:gallery_video].nil?
      user.videos.create(:file => params[:gallery_video])
    end
    render :json =>  {}
  end


  api :DELETE, '/account/removeGallery'
  formats ['json']
  param :gallery_id, String, :required => true
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
    == Response
    {}
    EOS
  def removeGallery
    g = current_user.pictures.where(:id => params[:gallery_id]).first
    if g.nil?
      g = current_user.videos.where(:id => params[:gallery_id]).first
    end
    if !g.nil?
      g.destroy
    end
    render :json => {}
  end


  api :GET, '/account/notifications'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  [
    {
      "id": "1km4l1km4lk1231",
      "description": "Você tem um novo job!",
      "time": "23/02/2018 12:03"
    },
    {
      "id": "1km4l1km4lk1231",
      "description": "Você tem um novo job!",
      "time": "23/02/2018 12:03"
    }
  ]
  EOS
  def notifications
    render :json => Notification.mapNotifications(current_user.notifications)
  end


  api :GET, '/account/getCharacteristics'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  {
    "height_in_cm": 183,
    "weight_in_kg": 78.3,
    "shoe_size": 43,
    "mannequin_size": null,
    "shirt_size": 5,
    "suit_size": 4,
    "bust_size": 100,
    "waist_size": 122,
    "hip_size": 132,
    "eye_color": [
      {
        "id": "123123123123",
        "name": "Azul",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Heterocromia (2 cores)",
        "isSelected": false
      }
    ],
    "observations": [
      {
        "id": "123123123123",
        "name": "Barba",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Bigode",
        "isSelected": false
      }
    ],
    "hair_style": [
      {
        "id": "123123123123",
        "name": "Multicolorido",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Ruivo",
        "isSelected": false
      }
    ],
    "hair_color": [
      {
        "id": "123123123123",
        "name": "Multicolorido",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Ruivo",
        "isSelected": false
      }
    ],
    "skin": [
      {
        "id": "123123123123",
        "name": "Branco",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Negro",
        "isSelected": false
      }
    ],
    "ethnicity": [
      {
        "id": "123123123123",
        "name": "Branco",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Negro",
        "isSelected": false
      }
    ],
    "languages": [
      {
        "id": "123123123123",
        "name": "Inglês",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Chinês",
        "isSelected": false
      }
    ],
    "accent": [
      {
        "id": "123123123123",
        "name": "Carioca",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Gaúcho",
        "isSelected": false
      }
    ],
    "gender_expressions": [
      {
        "id": "123123123123",
        "name": "Androgino",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "name": "Transgênero",
        "isSelected": false
      }
    ]
  }
  EOS
  def getCharacteristics
    render :json => UserApp.mapCharacteristics(AppearanceContent.all, current_user)
  end


  api :PUT, '/account/editCharacteristics'
  # param :height_in_cm, String, :required => false, :allow_nil => true
  # param :weight_in_kg, String, :required => false, :allow_nil => true
  # param :shoe_size, String, :required => false, :allow_nil => true
  # param :shirt_size, String, :required => false, :allow_nil => true
  # param :suit_size, String, :required => false, :allow_nil => true
  # param :bust_size, String, :required => false, :allow_nil => true
  # param :waist_size, String, :required => false, :allow_nil => true
  # param :hip_size, String, :required => false, :allow_nil => true
  param :opposite_gender_kiss, Boolean, :required => false, :allow_nil => true
  param :same_gender_kiss, Boolean, :required => false, :allow_nil => true
  param :naked_sex_cover, Boolean, :required => false, :allow_nil => true
  param :naked_apparent_breasts, Boolean, :required => false, :allow_nil => true
  param :extras, Boolean, :required => false, :allow_nil => true #Figurante
  param :outside_job, Boolean, :required => false, :allow_nil => true
  param :mannequin_size, String, :required => false, :allow_nil => true

  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == eye_color :array
  == observations :array
  == hair_style :array
  == hair_color :array
  == skin :array
  == ethnicity :array
  == languages :array
  == accent :array
  == gender_expressions :array

  == height_in_cm : Float
  == weight_in_kg :Float
  == shoe_size :Float
  == shirt_size :Float
  == suit_size :Float
  == bust_size :Float
  == waist_size :Float
  == hip_size :Float

  ==Response
  {
    "height_in_cm": 183,
    "weight_in_kg": 78.3,
    "shoe_size": 43,
    "mannequin_size": null,
    "shirt_size": 5,
    "suit_size": 4,
    "bust_size": 100,
    "waist_size": 122,
    "hip_size": 132,
    "eye_color": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "observations": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "hair_style": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "hair_color": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "skin": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "ethnicity": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "languages": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "accent": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ],
    "gender_expressions": [
      {
        "id": "123123123123",
        "isSelected": true
      },
      {
        "id": "1231k231k23m",
        "isSelected": false
      }
    ]
  }
  EOS
  def editCharacteristics
    user = UserApp.find(current_user.id)
    user.altura = params[:height_in_cm].to_f 
    user.peso = params[:weight_in_kg].to_f
    user.calcado = params[:shoe_size].to_f  
    user.camisa = params[:shirt_size].to_f 
    user.terno = params[:suit_size].to_f 
    user.busto = params[:bust_size].to_f 
    user.cintura = params[:waist_size].to_f 
    user.quadril = params[:hip_size].to_f 

    user.oppositeGenderKiss = params[:opposite_gender_kiss] 
    user.sameGenderKiss = params[:same_gender_kiss] 
    user.nakedSexCover = params[:naked_sex_cover] 
    user.nakedApparentBreasts = params[:naked_apparent_breasts] 
    user.aceitaFiguracao = params[:extras] 
    user.outsideJob = params[:outside_job] 

    user.mannequin_size_id = params[:mannequin_size].blank? ? nil : params[:mannequin_size]

    # camiseta ??
    user.appearance_content_ids = nil

    if params[:eye_color]
      params[:eye_color].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:observations]
      params[:observations].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:hair_style]
      params[:hair_style].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:hair_color]
      params[:hair_color].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:skin]
      params[:skin].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:ethnicity]
      params[:ethnicity].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:languages]
      params[:languages].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:accent]
      params[:accent].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end

    if params[:gender_expressions]
      params[:gender_expressions].each { |p|
        if p[:isSelected] == true
          user.appearance_content_ids += [p[:id].to_s]
        end
      }
    end
    user.save(validate:false)
    render :json => UserApp.mapCharacteristics(AppearanceContent.all, user)
  end



  api :GET, '/account/getFirstContact'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  ==Response
  {
    "full_name": "Jose Bruno Silva",
    "artistic_name": "JB",
    "gender": null,
    "date_limit": "23/02/2000",
    "birth_date": "12/06/1980",
    "responsible_name": "Nome do responsável",
    "picture": "http://texashillcountry.com/wp-content/uploads/15823441_10154676440151539_565849787411397170_n-660x400.jpg",
    "responsible_cpf": "12312312312",
    "email": "jose@mobile2you.com.br",
    "phones": [
      {
        "id": "0",
        "phone": "119531569795"
      },
      {
        "id": "1",
        "phone": "119531569795"
      }
    ],
    "ocupation": [
      {
        "id": "1",
        "name": "figurante",
        "isSelected": true
      },
      {
        "id": "2",
        "name": "ator com experiencia em publicidade",
        "isSelected": true
      },
      {
        "id": "3",
        "name": "ator com experiencia em tv",
        "isSelected": true
      },
      {
        "id": "4",
        "name": "ator com experiencia em cinema",
        "isSelected": false
      },
      {
        "id": "5",
        "name": "ator com experiencia em teatro",
        "isSelected": false
      },
      {
        "id": "6",
        "name": "ator com experiencia em teatro musical",
        "isSelected": false
      },
      {
        "id": "7",
        "name": "ator ainda em formação",
        "isSelected": false
      },
      {
        "id": "8",
        "name": "ator comediante",
        "isSelected": false
      },
      {
        "id": "9",
        "name": "ator de improviso",
        "isSelected": false
      },
      {
        "id": "10",
        "name": "Atleta profissional",
        "isSelected": false
      },
      {
        "id": "11",
        "name": "Atleta AMADOR",
        "isSelected": false
      },
      {
        "id": "12",
        "name": "Atleta paralimpico",
        "isSelected": false
      },
      {
        "id": "13",
        "name": "modelo fashion",
        "isSelected": false
      },
      {
        "id": "14",
        "name": "modelo publicitário",
        "isSelected": false
      },
      {
        "id": "15",
        "name": "modelo passarela",
        "isSelected": false
      },
      {
        "id": "16",
        "name": "modelo fitness",
        "isSelected": false
      },
      {
        "id": "17",
        "name": "MODELO DE CORPO",
        "isSelected": false
      },
      {
        "id": "18",
        "name": "modelo plus size",
        "isSelected": false
      },
      {
        "id": "19",
        "name": "MODELO DE ROSTO",
        "isSelected": false
      },
      {
        "id": "20",
        "name": "modelo detalhe de cabelo",
        "isSelected": false
      },
      {
        "id": "21",
        "name": "modelo detalhe de mão",
        "isSelected": false
      },
      {
        "id": "22",
        "name": "modelo detalhe de pé",
        "isSelected": false
      },
      {
        "id": "23",
        "name": "modelo detalhe de pernas",
        "isSelected": false
      },
      {
        "id": "24",
        "name": "modelo detalhe de boca/dentes",
        "isSelected": false
      },
      {
        "id": "25",
        "name": "Especial FISICA",
        "isSelected": false
      },
      {
        "id": "26",
        "name": "Especial AUDITIVA",
        "isSelected": false
      },
      {
        "id": "27",
        "name": "Especial MENTAL",
        "isSelected": false
      },
      {
        "id": "28",
        "name": "Especial MOTORA",
        "isSelected": false
      },
      {
        "id": "29",
        "name": "Especial VISUAL",
        "isSelected": false
      },
      {
        "id": "30",
        "name": "MUSICO",
        "isSelected": false
      },
      {
        "id": "31",
        "name": "Artista de dança",
        "isSelected": false
      },
      {
        "id": "32",
        "name": "Artista de CIRCO",
        "isSelected": false
      },
      {
        "id": "33",
        "name": "Digital Influencer Youtube",
        "isSelected": false
      },
      {
        "id": "34",
        "name": "Digital Influencer insta",
        "isSelected": false
      },
      {
        "id": "35",
        "name": "Digital Influencer blog",
        "isSelected": false
      },
      {
        "id": "36",
        "name": "Digital Influencer insta",
        "isSelected": false
      }
    ]
  }
  EOS
  def getFirstContact
    if !current_user.nil?
      render :json => UserApp.mapFirstContact(current_user)
    else
      render :json => UserApp.mapFirstContactNonLogin()
    end
  end


  # Create First Contact
  api :POST, '/account/createFirstContact'
  param :full_name, String, :required => false, :allow_nil => true
  param :artistic_name, String, :required => false, :allow_nil => true
  param :gender, String, :required => false, :allow_nil => true
  param :birth_date, String, :required => false, :allow_nil => true
  param :email, String, :required => false, :allow_nil => true
  param :password, String, :required => false, :allow_nil => true
  param :responsible_cpf, String, :required => false, :allow_nil => true
  param :responsible_name, String, :required => false, :allow_nil => true
  formats ['json']
  error 401, "Usuário não autenticado"
  error 403, "E-mail já cadastrado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == picture  :multipart
  == phones :array[String]
  == ocupation :array[ID]
  ==Body
  {
    "full_name": "Jose Bruno Silva",
    "artistic_name": "JB",
    "gender": null,
    "birth_date": "12/06/1980",
    "email": "jose@mobile2you.com.br",
    "phones": [
      {
        "id": "0",
        "phone": "119531569795"
      },
      {
        "id": "1",
        "phone": "119531569795"
      },
      {
        "id": "2",
        "phone": "asdasdasdas"
      }
    ],
    "ocupation": [
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7"
    ]
  }
  ==Response
  {
    "full_name": "Jose Bruno Silva",
    "artistic_name": "JB",
    "gender": null,
    "date_limit": "23/02/2000",
    "birth_date": "12/06/1980",
    "responsible_name": "Nome do responsável",
    "picture": "http://texashillcountry.com/wp-content/uploads/15823441_10154676440151539_565849787411397170_n-660x400.jpg",
    "responsible_cpf": "12312312312",
    "email": "jose@mobile2you.com.br",
    "phones": [
      {
        "id": "0",
        "phone": "119531569795"
      },
      {
        "id": "1",
        "phone": "119531569795"
      }
    ],
    "ocupation": [
      {
        "id": "1",
        "name": "figurante",
        "isSelected": true
      },
      {
        "id": "2",
        "name": "ator com experiencia em publicidade",
        "isSelected": true
      },
      {
        "id": "3",
        "name": "ator com experiencia em tv",
        "isSelected": true
      },
      {
        "id": "4",
        "name": "ator com experiencia em cinema",
        "isSelected": false
      },
      {
        "id": "5",
        "name": "ator com experiencia em teatro",
        "isSelected": false
      },
      {
        "id": "6",
        "name": "ator com experiencia em teatro musical",
        "isSelected": false
      },
      {
        "id": "7",
        "name": "ator ainda em formação",
        "isSelected": false
      },
      {
        "id": "8",
        "name": "ator comediante",
        "isSelected": false
      },
      {
        "id": "9",
        "name": "ator de improviso",
        "isSelected": false
      },
      {
        "id": "10",
        "name": "Atleta profissional",
        "isSelected": false
      },
      {
        "id": "11",
        "name": "Atleta AMADOR",
        "isSelected": false
      },
      {
        "id": "12",
        "name": "Atleta paralimpico",
        "isSelected": false
      },
      {
        "id": "13",
        "name": "modelo fashion",
        "isSelected": false
      },
      {
        "id": "14",
        "name": "modelo publicitário",
        "isSelected": false
      },
      {
        "id": "15",
        "name": "modelo passarela",
        "isSelected": false
      },
      {
        "id": "16",
        "name": "modelo fitness",
        "isSelected": false
      },
      {
        "id": "17",
        "name": "MODELO DE CORPO",
        "isSelected": false
      },
      {
        "id": "18",
        "name": "modelo plus size",
        "isSelected": false
      },
      {
        "id": "19",
        "name": "MODELO DE ROSTO",
        "isSelected": false
      },
      {
        "id": "20",
        "name": "modelo detalhe de cabelo",
        "isSelected": false
      },
      {
        "id": "21",
        "name": "modelo detalhe de mão",
        "isSelected": false
      },
      {
        "id": "22",
        "name": "modelo detalhe de pé",
        "isSelected": false
      },
      {
        "id": "23",
        "name": "modelo detalhe de pernas",
        "isSelected": false
      },
      {
        "id": "24",
        "name": "modelo detalhe de boca/dentes",
        "isSelected": false
      },
      {
        "id": "25",
        "name": "Especial FISICA",
        "isSelected": false
      },
      {
        "id": "26",
        "name": "Especial AUDITIVA",
        "isSelected": false
      },
      {
        "id": "27",
        "name": "Especial MENTAL",
        "isSelected": false
      },
      {
        "id": "28",
        "name": "Especial MOTORA",
        "isSelected": false
      },
      {
        "id": "29",
        "name": "Especial VISUAL",
        "isSelected": false
      },
      {
        "id": "30",
        "name": "MUSICO",
        "isSelected": false
      },
      {
        "id": "31",
        "name": "Artista de dança",
        "isSelected": false
      },
      {
        "id": "32",
        "name": "Artista de CIRCO",
        "isSelected": false
      },
      {
        "id": "33",
        "name": "Digital Influencer Youtube",
        "isSelected": false
      },
      {
        "id": "34",
        "name": "Digital Influencer insta",
        "isSelected": false
      },
      {
        "id": "35",
        "name": "Digital Influencer blog",
        "isSelected": false
      },
      {
        "id": "36",
        "name": "Digital Influencer insta",
        "isSelected": false
      }
    ]
  }
  EOS
  def createFirstContact
    u = UserApp.where(:email => params[:email].downcase).first
    if !u.nil?
      render :json =>  {:message => "E-mail já cadastrado"}, :status => 403
    else
      user = UserApp.new
      user.name = params[:full_name] 
      user.artisticName = params[:artistic_name] 
      user.gender = params[:gender]
      user.birthday = params[:birth_date] 
      user.responsibleCpf = params[:responsible_cpf] 
      user.responsibleName = params[:responsible_name]
      user.email = params[:email].downcase
      user.password = params[:password]
      user.password_confirmation = params[:password]
      user.save(validate: false)

      if !params[:picture].nil?
        user.picture = params[:picture]
      end

      if params[:phones]
        params[:phones].each { |phone|
          user.phones.create(number: phone)
        }
      end

      if params[:ocupation]
        params[:ocupation].each { |performance|
          user.performance_content_ids << performance
        }
      end

      user.save(validate: false)
      sign_in user, :bypass => true

      ContactMailer.user_subscribe(user).deliver
      render :json => UserApp.mapFirstContact(user)
    end
  end


  # Edit First Contact
  api :PUT, '/account/editFirstContact'
  param :email, String, :required => true
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == phones
  == ocupation
  ==Body
  {
    "full_name": "Jose Bruno Silva",
    "artistic_name": "JB",
    "gender": null,
    "birth_date": "12/06/1980",
    "email": "jose@mobile2you.com.br",
    "phones": [
      {
        "id": "0",
        "phone": "119531569795"
      },
      {
        "id": "1",
        "phone": "119531569795"
      },
      {
        "id": "2",
        "phone": "asdasdasdas"
      }
    ],
    "ocupation": [
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7"
    ]
  }
  ==Response
  {
    "full_name": "Jose Bruno Silva",
    "artistic_name": "JB",
    "gender": null,
    "birth_date": "12/06/1980",
    "email": "jose@mobile2you.com.br",
    "phones": [
      {
        "id": "0",
        "phone": "119531569795"
      },
      {
        "id": "1",
        "phone": "119531569795"
      }
    ],
    "ocupations": [
      {
        "id": "1",
        "name": "figurante",
        "isSelected": true
      },
      {
        "id": "2",
        "name": "ator com experiencia em publicidade",
        "isSelected": true
      },
      {
        "id": "3",
        "name": "ator com experiencia em tv",
        "isSelected": true
      },
      {
        "id": "4",
        "name": "ator com experiencia em cinema",
        "isSelected": false
      },
      {
        "id": "5",
        "name": "ator com experiencia em teatro",
        "isSelected": false
      },
      {
        "id": "6",
        "name": "ator com experiencia em teatro musical",
        "isSelected": false
      },
      {
        "id": "7",
        "name": "ator ainda em formação",
        "isSelected": false
      },
      {
        "id": "8",
        "name": "ator comediante",
        "isSelected": false
      },
      {
        "id": "9",
        "name": "ator de improviso",
        "isSelected": false
      },
      {
        "id": "10",
        "name": "Atleta profissional",
        "isSelected": false
      },
      {
        "id": "11",
        "name": "Atleta AMADOR",
        "isSelected": false
      },
      {
        "id": "12",
        "name": "Atleta paralimpico",
        "isSelected": false
      },
      {
        "id": "13",
        "name": "modelo fashion",
        "isSelected": false
      },
      {
        "id": "14",
        "name": "modelo publicitário",
        "isSelected": false
      },
      {
        "id": "15",
        "name": "modelo passarela",
        "isSelected": false
      },
      {
        "id": "16",
        "name": "modelo fitness",
        "isSelected": false
      },
      {
        "id": "17",
        "name": "MODELO DE CORPO",
        "isSelected": false
      },
      {
        "id": "18",
        "name": "modelo plus size",
        "isSelected": false
      },
      {
        "id": "19",
        "name": "MODELO DE ROSTO",
        "isSelected": false
      },
      {
        "id": "20",
        "name": "modelo detalhe de cabelo",
        "isSelected": false
      },
      {
        "id": "21",
        "name": "modelo detalhe de mão",
        "isSelected": false
      },
      {
        "id": "22",
        "name": "modelo detalhe de pé",
        "isSelected": false
      },
      {
        "id": "23",
        "name": "modelo detalhe de pernas",
        "isSelected": false
      },
      {
        "id": "24",
        "name": "modelo detalhe de boca/dentes",
        "isSelected": false
      },
      {
        "id": "25",
        "name": "Especial FISICA",
        "isSelected": false
      },
      {
        "id": "26",
        "name": "Especial AUDITIVA",
        "isSelected": false
      },
      {
        "id": "27",
        "name": "Especial MENTAL",
        "isSelected": false
      },
      {
        "id": "28",
        "name": "Especial MOTORA",
        "isSelected": false
      },
      {
        "id": "29",
        "name": "Especial VISUAL",
        "isSelected": false
      },
      {
        "id": "30",
        "name": "MUSICO",
        "isSelected": false
      },
      {
        "id": "31",
        "name": "Artista de dança",
        "isSelected": false
      },
      {
        "id": "32",
        "name": "Artista de CIRCO",
        "isSelected": false
      },
      {
        "id": "33",
        "name": "Digital Influencer Youtube",
        "isSelected": false
      },
      {
        "id": "34",
        "name": "Digital Influencer insta",
        "isSelected": false
      },
      {
        "id": "35",
        "name": "Digital Influencer blog",
        "isSelected": false
      },
      {
        "id": "36",
        "name": "Digital Influencer insta",
        "isSelected": false
      }
    ]
  }
  EOS
  def editFirstContact
    user = UserApp.find(current_user.id)
    user.name = params[:full_name] 
    user.artisticName = params[:artistic_name] 
    user.gender = params[:gender]
    user.birthday = params[:birth_date]
    user.email = params[:email].downcase
    if !params[:password].blank?
      user.password = params[:password]
      user.password_confirmation = params[:password]
    end

    if params[:phones]
      user.phones.delete_all
      params[:phones].each { |phone|
        user.phones.create(number: phone)
      }
    end

    if params[:ocupation]
      user.performance_content_ids = nil
      params[:ocupation].each { |performance|
        user.performance_content_ids << performance
      }
    end

    user.save(validate:false)
    render :json => UserApp.mapFirstContact(user)
  end


  #Add addPicture 
  api :POST, '/account/addPicture'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == picture :multipart
  EOS
  def addPicture
    user = UserApp.find(current_user.id)

    if !params[:picture].nil?
      user.picture = params[:picture]
    end

    user.changedPhoto = true
    user.save(validate: false)
    render :json => {}# current_user.picture.url
  end


  #Remove removePicture
  api :DELETE, '/account/removePicture'
  param :picture_id, String, :required => true
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  {}
  EOS
  def removePicture
    # g = current_user.pictures.where(:id => params[:picture_id])
    # if g.nil?
    #   g = current_user.videos.where(:id => params[:picture_id])
    # end
    # if !g.nil?
    #   g.destroy
    # end

    user = UserApp.find(current_user.id)
    user.picture = nil
    user.save(validate:false)
    render :json => {}
  end


  #Get Skills
  api :GET, '/account/getSkills'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  [
    {
      "name": "Habilidade de Circo",
      "id": "0",
      "options": [
        {
          "id": "0",
          "name": "Malabarismo",
          "status": 0
        },
        {
          "id": "1",
          "name": "Trampolim",
          "status": 1
        }
      ]
    },
    {
      "name": "Habilidade de Dança",
      "id": "1",
      "options": [
        {
          "id": "0",
          "name": "Forró",
          "status": 0
        },
        {
          "id": "1",
          "name": "Foxtrot",
          "status": 1
        },
        {
          "id": "2",
          "name": "Frevo",
          "status": 2
        }
      ]
    },
    {
      "name": "Habilidade de Direção/Pilotagem",
      "id": "0",
      "options": [
        {
          "id": "0",
          "name": "Cavalo",
          "status": 0
        },
        {
          "id": "1",
          "name": "Drone",
          "status": 1
        },
        {
          "id": "2",
          "name": "Navio",
          "status": 1
        }
      ]
    }
  ]
  EOS
  def getSkills
    render :json => Skill.mapSkill(Skill.all, current_user)
  end

  #Edit Skills
  api :PUT, '/account/editSkills'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == skills[{options[{option_id, status}, {String, Integer}]}]: array
  == Response
  [
    {
      "name": "Habilidade de Circo",
      "id": "0",
      "options": [
        {
          "id": "0",
          "name": "Malabarismo",
          "status": 0
        },
        {
          "id": "1",
          "name": "Trampolim",
          "status": 1
        }
      ]
    },
    {
      "name": "Habilidade de Dança",
      "id": "1",
      "options": [
        {
          "id": "0",
          "name": "Forró",
          "status": 0
        },
        {
          "id": "1",
          "name": "Foxtrot",
          "status": 1
        },
        {
          "id": "2",
          "name": "Frevo",
          "status": 2
        }
      ]
    },
    {
      "name": "Habilidade de Direção/Pilotagem",
      "id": "0",
      "options": [
        {
          "id": "0",
          "name": "Cavalo",
          "status": 0
        },
        {
          "id": "1",
          "name": "Drone",
          "status": 1
        },
        {
          "id": "2",
          "name": "Navio",
          "status": 1
        }
      ]
    }
  ]
  EOS
  def editSkills
    current_user.skill_contents_prof.each do |d|
      current_user.skill_contents_prof.delete(d)
    end
    current_user.skill_contents_jr.each do |d|
      current_user.skill_contents_jr.delete(d)
    end

    if !params[:skills].nil?
      params[:skills].each do |skill|
        if !skill[:options].blank?
          skill[:options].each do |d|
            r = SkillContent.find(d[:option_id].to_s)
            if d[:status].to_i == 1 #profissional
              r.users_jr << current_user
              r.users_prof.delete(current_user)
            elsif d[:status].to_i == 2 #amador
              r.users_prof << current_user
              r.users_jr.delete(current_user)
            elsif d[:status].to_i == 0 
              r.users_prof.delete(current_user)
              r.users_jr.delete(current_user)
            end
          end
        end
      end
    end
    render :json => Skill.mapSkill(Skill.all, current_user)
  end


  #Get Person Data
  api :GET, '/account/getPersonalData'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  {
    "id": "5a58b9738a21b1a118000003",
    "rg": "22.233.332-8",
    "cpf": "",
    "issuer_date": "",
    "issuer": "",
    "address_country": "",
    "address_state": "",
    "address_city": "",
    "curriculum": "",
    "address_district": "",
    "address_complement": "",
    "address_number": "",
    "address": "",
    "issuer_uf": "",
    "cnh_expiration_date": "",
    "cnh_category": [
        {
            "id": "5a946cc47b44fedb30000004",
            "name": "A",
            "isSelected": false
        },
        {
            "id": "5a946ccb7b44fedb30000006",
            "name": "B",
            "isSelected": true
        }
    ],
    "passport_nationality": [
        {
            "id": "5a946dcb7b44fedb30000009",
            "name": "Brasileiro",
            "isSelected": false
        },
        {
            "id": "5a946de17b44fedb3000000b",
            "name": "Argentino",
            "isSelected": true
        },
        {
            "id": "5a946df67b44fedb3000000d",
            "name": "Americano",
            "isSelected": false
        }
    ],
    "passport_expiration_date": "",
    "schooling": [
        {
            "id": "5a946e497b44fedb30000014",
            "name": "Ensino Fundamental",
            "isSelected": true
        },
        {
            "id": "5a946e7e7b44fedb30000016",
            "name": "Graduação",
            "isSelected": false
        }
    ],
    "origin": [
        {
            "id": "5a946e1c7b44fedb30000010",
            "name": "Brasil",
            "isSelected": true
        },
        {
            "id": "5a946e2a7b44fedb30000012",
            "name": "Canadá",
            "isSelected": false
        }
    ],
    "nationality": [
        {
            "id": "5a946dcb7b44fedb30000009",
            "name": "Brasileiro",
            "isSelected": false
        },
        {
            "id": "5a946de17b44fedb3000000b",
            "name": "Argentino",
            "isSelected": true
        },
        {
            "id": "5a946df67b44fedb3000000d",
            "name": "Americano",
            "isSelected": false
        }
    ]
  }
  EOS
  def getPersonalData
    render :json => UserApp.mapPersonalData(current_user)
  end


  #Edit Person Data
  api :PUT, '/account/editPersonalData'
  param :rg, String, :required => false, :allow_nil => true
  param :cpf, String, :required => false, :allow_nil => true
  param :issuer_date, String, :required => false, :allow_nil => true
  param :issuer, String, :required => false, :allow_nil => true
  param :issuer_uf, String, :required => false, :allow_nil => true
  param :address_country, String, :required => false, :allow_nil => true
  param :address_state, String, :required => false, :allow_nil => true
  param :address_city, String, :required => false, :allow_nil => true
  param :address_district, String, :required => false, :allow_nil => true
  param :address_complement, String, :required => false, :allow_nil => true
  param :address_number, String, :required => false, :allow_nil => true
  param :address, String, :required => false, :allow_nil => true
  param :cnh_expiration_date, String, :required => false, :allow_nil => true
  param :passport_expiration_date, String, :required => false, :allow_nil => true
  param :schooling, String, :required => false, :allow_nil => true
  # param :nationality, String, :required => false, :allow_nil => true
  # param :opposite_gender_kiss, String, :required => false
  # param :same_gender_kiss, String, :required => false
  # param :naked_sex_cover, String, :required => false
  # param :naked_apparent_breasts, String, :required => false
  # param :extras, String, :required => false
  # param :outside_job, String, :required => false

  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == cnh_category :array
  == passport_nationality :array
  == nationality :array
  == origin :array
  == file :multipart

  == Response
  {
    "rg": "123123123123",
    "cpf": "1231231213",
    "issuer_date": "Hello World.",
    "issuer": "Hello World.",
    "issuer_uf": "Hello World.",
    "address_country": "322343432",
    "address_state": "São Paulo",
    "address_city": "São Paulo",
    "address_district": "Santo Amaro",
    "address_complement": "Torre 2",
    "address_number": "123",
    "address": "Alameda Itu",
    "cnh_expiration_date": "Hello World.",
    "cnh_category": [
      "1",
      "2",
      "3",
    ],
    "passport_nationality": [
      "1",
      "2",
      "7"
    ],
    "passport_expiration_date": "12/20/2004",
    "schooling": "1",
    "origin": "1",
    "nationality": "1"
  }
  EOS
  def editPersonalData
    user = UserApp.find(current_user.id)
    user.rg = params[:rg].blank? ? "" : params[:rg]
    user.cpf = params[:cpf].blank? ? "" : params[:cpf]
    user.issuerDate = params[:issuer_date].blank? ? "" : params[:issuer_date]
    user.issuer = params[:issuer].blank? ? "" : params[:issuer]
    user.issuerUF = params[:issuer_uf].blank? ? "" : params[:issuer_uf]
    user.addressCountry_id = params[:address_country].blank? ? nil : params[:address_country]
    user.addressState = params[:address_state].blank? ? "" : params[:address_state]
    user.addressCity = params[:address_city].blank? ? "" : params[:address_city]
    user.addressDistrict = params[:address_district].blank? ? "" : params[:address_district]
    user.addressComplement = params[:address_complement].blank? ? "" : params[:address_complement]
    user.addressNumber = params[:address_number].blank? ? "" : params[:address_number]
    user.address = params[:address].blank? ? "" : params[:address]
    user.cnhExpireDate = params[:cnh_expiration_date].blank? ? "" : params[:cnh_expiration_date]
    user.passportExpireDate = params[:passport_expiration_date].blank? ? "" : params[:passport_expiration_date]
    user.schooling_id = params[:schooling].blank? ? nil : params[:schooling]
    user.origin_country_id = params[:origin].blank? ? nil : params[:origin]

    if params[:file]
      user.curriculum = params[:file]
    end

    user.cnhs = []
    if !params[:cnh_category].blank?
      params[:cnh_category].each { |p|
        if p[:isSelected] == true
          user.cnh_ids += [p[:id].to_s]
        end
      }
    end

    user.passport_nationalities = []
    if !params[:passport_nationality].blank?
      params[:passport_nationality].each { |p|
        if p[:isSelected] == true
          user.passport_nationality_ids += [p[:id].to_s]
        end
      }
    end

    user.nationality = nil
    if !params[:nationality].blank?
      params[:nationality].each { |p|
        if p[:isSelected] == true
          user.nationality = p[:id].to_s
        end
      }
    end

    user.save(validate:false)
    render :json => UserApp.mapPersonalData(user)
  end


  #Add Curriculum
  api :POST, '/account/addCurriculum'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == file :multipart
  {}
  EOS
  def addCurriculum
    user = UserApp.find(current_user.id)
    user.curriculum = params[:file]
    user.save(validate:false)
    render :json => {}
  end


  #Remove Curriculum
  api :DELETE, '/account/removeCurriculum'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  {}
  EOS
  def removeCurriculum
    user = UserApp.find(current_user.id)
    user.curriculum = nil
    user.save(validate:false)
    render :json => {}
  end



  #Get Social Network
  api :GET, '/account/getSocialNetworks'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  {
    "facebook": "facebooklink",
    "instagram": "12345678",
    "linkedin": "asdas",
    "twitter": "@twitter",
    "skype": "skypename"
  }
  EOS
  def getSocialNetworks
    render :json => UserApp.mapSocialNetworks(current_user)
  end


  #Edit Social Network
  api :PUT, '/account/editSocialNetworks'
  param :facebook, String, :required => false, :allow_nil => true
  param :instagram, String, :required => false, :allow_nil => true
  param :linkedin, String, :required => false, :allow_nil => true
  param :twitter, String, :required => false, :allow_nil => true
  param :skype, String, :required => false, :allow_nil => true
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  {
    "facebook": "facebooklink",
    "instagram": "12345678",
    "linkedin": "asdas",
    "twitter": "@twitter",
    "skype": "skypename"
  }
  EOS
  def editSocialNetworks
    user = UserApp.find(current_user.id)
    user.facebook = params[:facebook]
    user.instagram = params[:instagram]
    user.linkedin = params[:linkedin]
    user.twitter = params[:twitter]
    user.skype = params[:skype]
    user.save(validate:false)
    render :json => UserApp.mapSocialNetworks(user)
  end
end
