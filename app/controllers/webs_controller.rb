class WebsController < WebApplicationController

  respond_to :html

  def index
    @portfolio = Portfolio.all
  end

  def user_signup
    # @AppearanceContent = AppearanceContent.any_of({:parent_name => ""},{:parent_name => ""})
    @performances = PerformanceContent.all
    @appearances = AppearanceContent.all
    @skills = SkillContent.all
    @user = nil#UserApp.all.first
  end

  def manage_jobs
    # @jobs = Job.where(:user_producer => current_user)
    @jobs = Job.all #remover
  end

  def get_user_perfil
    user = UserApp.find(params[:user])
    job = Job.find(params[:job])

    status = Status.mapStatus(Status.all.asc(:position), user.status_user_jobs, job)
    questions = Job.mapQuestions(job.questions.where(:type.in => [0, 1]), user)

    #0 - texto, 1 - link, 2 - imagem,3 - video
    pictures = []
    if !job.questions.blank?
      job.questions.where(:type => 2).each do |question|
        pictures << Job.getAnswer(2, question.answers.where(:user_app_id => user.id).first)
      end
    end

    videos = []
    if !job.questions.blank?
      job.questions.where(:type => 3).each do |question|
        videos << Job.getAnswer(3, question.answers.where(:user_app_id => user.id).first)
      end
    end

    render :json => {:user => user, 
      :picture => user.picture.to_s,
      :status => status, 
      :questions => questions,
      :pictures => pictures,
      :videos => videos}
  end

  def new_job
    #Carecteristicas
    appearances = AppearanceContent.all
    @CorOlhos = appearances.where(:parent_name => "Cor dos Olhos")
    @CorCabelos = appearances.where(:parent_name => "Cor dos Cabelos")
    @TipoFisico = appearances.where(:parent_name => "Tipo Físico")
    @TipoCabelo = appearances.where(:parent_name => "Tipo de Cabelo")
    @Idiomas = appearances.where(:parent_name => "Idiomas")
    @Sotaque = appearances.where(:parent_name => "Sotaque")
    @ExpGenero = appearances.where(:parent_name => "Expressão de Gênero")


    #Habilidades
    @skills = SkillContent.all
  end

#cria Job de Pesquisa pela Web
  def open_job

  end

  def edit_open_job
    @job  = Job.find(params[:id])
  end

  def search_users
    @job = Job.where(:id => params[:job]).first

    @users = []
    search_ids = []
    search = []

    if !@job.nil?
      search = @job.desc.split(" ")

      # skills
      search_ids = @job.skill_contents_prof_ids
      contents = SkillContent.any_of({:id.in => search_ids}, {:name.in => search}, {:parent_name.in => search})
      contents.each do |s|
        if s.user_prof.count > 0
          s.user_prof.each do |r|
            if !@users.include?(r)
              @users << r
            end
          end
        end
      end

      search_ids = @job.skill_contents_jr_ids
      contents = SkillContent.any_of({:id.in => search_ids}, {:name.in => search}, {:parent_name.in => search})
      contents.each do |s|
        if s.users_jr.count > 0
          s.users_jr.each do |r|
            if !@users.include?(r)
              @users << r
            end
          end
        end
      end

      # Performance
      search_ids = @job.performance_content_ids
      contents = PerformanceContent.any_of({:id.in => search_ids}, {:name.in => search}, {:parent_name.in => search})
      contents.each do |s|
        if s.user_apps.count > 0
          s.user_apps.each do |r|
            if !@users.include?(r)
              @users << r
            end
          end
        end
      end
      # Appearance
      search_ids = @job.appearance_content_ids
      contents = AppearanceContent.any_of({:id.in => search_ids}, {:name.in => search}, {:parent_name.in => search})
      contents.each do |s|
        if s.user_apps.count > 0
          s.user_apps.each do |r|
            if !@users.include?(r)
              @users << r
            end
          end
        end
      end
      # @users = @users.inject(Hash.new(0)) { |h, e| h[e] +=1 ; h } #Traz o resultado ordenado por vezes de relevancia que ele foi encontrado na busca
    else
      redirect_to "/gerenciar"
    end

    if @users.count == 0
      @users = UserApp.all.desc(:created_at)
    end
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
  end

  def update
  end

  def destroy
  end

#inscricao de Job Pesquisa pela web
  def open_job_subscribe
    @job = Job.find(params[:job])
  end

  #mostrar perfil publico da pesquisa
  def public_perfil
    @user = UserApplicant.find(params[:perfil])
  end

  def edit_public_perfil
    @user = UserApplicant.find(params[:perfil])
  end

  def download
    file = Picture.find(params[:file_id])

    data = open(file) 
    send_data data.read #, filename: "NAME YOU WANT.pdf", type: "application/pdf", disposition: 'inline', stream: 'true', buffer_size: '4096' 
  end 
  # def download
  #   send_file params[:file_name]
  # end

end
