class Status
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :title, type: String
  field :desc, type: String
  field :type, type: Integer
  field :position, type: Integer

  # has_many :status_user_jobs

#model Job >mapJob, >mapJobs
  def self.mapStatus (array, c_user_statuses, job)
    array.map{|status|{ 
      :id => status.id,
      :title => status.title,
      :description => status.position == 4 ? self.statusDesc(status.desc, job) : status.desc,
      :type => status.type,
      :position => status.position,
      :complete => c_user_statuses.blank? ? false : c_user_statuses.where(:status_id => status.id, :job_id => job.id).first.complete
    }}
  end

  def self.statusDesc(desc, job)
    if !desc.blank?
    	desc.gsub("%data%", job.recording_date.blank? ? "" : job.recording_date).gsub("%horario%", job.recording_time.blank? ? "" : job.recording_time).gsub("%local%", job.recording_place.nil? ? "" : job.recording_place)
    end
  end
end
