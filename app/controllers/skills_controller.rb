class SkillsController < ApplicationController
  before_filter :set_skill, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    @skills = Skill.all
    respond_with(@skills)
  end

  def show
    respond_with(@skill)
  end

  def new
    @skill = Skill.new
    respond_with(@skill)
  end

  def edit
  end

  def create
    @skill = Skill.new(params[:skill])
    flash[:notice_success] = 'Criado com sucesso.' if @skill.save
    respond_with(@skill)
  end

  def update
    flash[:notice_success] = 'Atualizado com sucesso.' if @skill.update_attributes(params[:skill])
    respond_with(@skill)
  end

  def destroy
    @skill.destroy
    respond_with(@skill)
  end

  private
    def set_skill
      @skill = Skill.find(params[:id])
    end
end
