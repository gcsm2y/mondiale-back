class PicturesController < ApplicationController
  before_filter :set_picture, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @pictures = Picture.all
    respond_with(@pictures)
  end

  def show
    respond_with(@picture)
  end

  def new
    @picture = Picture.new
    respond_with(@picture)
  end

  def edit
  end

  def create
    @picture = Picture.new(params[:picture])
    flash[:notice_success] = 'Criado com sucesso.' if @picture.save
    respond_with(@picture)
  end

  def update
    flash[:notice_success] = 'Atualizado com sucesso.' if @picture.update_attributes(params[:picture])
    respond_with(@picture)
  end

  def destroy
    p = @picture.event
    if !p.nil?
      @picture.image = nil
      @picture.save
      redirect_to edit_event_path(p)
    else
      s = @picture.sponsor_id
      @picture.destroy
      redirect_to edit_sponsor_path(s)
    end

    # respond_with(p)
    # respond_with(@picture)
  end

  private
    def set_picture
      @picture = Picture.find(params[:id])
    end
end
