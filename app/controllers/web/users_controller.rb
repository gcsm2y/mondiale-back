require 'open-uri'
class Web::UsersController < ApplicationController

  def download
    file = Picture.where(:id => params[:file_id])
    if file.blank?
      file = Answer.where(:id => params[:file_id])
      if file.blank?
        file = Video.find(params[:file_id])
      else
        file = file.first
      end
    else
      file = file.first
    end
    begin
      data = open(file.file.to_s) 
      send_data data.read , filename: params[:name]
    rescue
      render :json => {:msg => "Ocorreu um erro"}
    end
  end 

  api :POST, '/users/signin'
  def signin
    u = User.where(:email => params[:email]).first
    if !u.nil?
      if u.valid_password?(params[:password]) 
        sign_in u, :bypass => true
        u.save(validate: false)

        if u.class == UserApp
          redirect_to "/signup"
        elsif u.class == UserProducer
          redirect_to "/gerenciar"
        elsif u.class == SuperAdmin
          redirect_to "/admin"
        end
      else
        render :json => {:message => "Senha incorreta"}, :status => 403
      end
    else
      render :json => {:message => "E-mail não cadastrado"}, :status => 404
    end
  end

  api :POST, '/users/new'
	def new
    user = UserApp.new(:picture => params[:picture],
                       :login => params[:login],
                       :password => params[:password],
                       :name => params[:name],
                       :birthday => params[:birthday],
                       :gender => params[:gender],
                       :country => params[:country],
                       :state => params[:state],
                       :city => params[:city],
                       :email => params[:email],
                       :phone => params[:phone],

                       :email2 => params[:email2],
                       :skype => params[:skype],
                       :website => params[:website],
                       :cellphone => params[:cellphone],
                       :razaoSocial => params[:razaoSocial],
                       :cnpj => params[:cnpj],
                       :inscEstadual => params[:inscEstadual],
                       :inscMunicipal => params[:inscMunicipal],
                       :emailCompany => params[:emailCompany],
                       :phone1Company => params[:phone1Company],
                       :phone2Company => params[:phone2Company],
                       :typeCompany => params[:typeCompany],

                       :altura => params[:altura],
                       :peso => params[:peso],
                       :calcado => params[:calcado],
                       :manequim => params[:manequim],
                       :camisa => params[:camisa],
                       :terno => params[:terno],
                       :camiseta => params[:camiseta],
                       :busto => params[:busto],
                       :cintura => params[:cintura],
                       :quadril => params[:quadril],
                       :aceitaFiguracao => params[:figuracao]
                       )

    # user.performance_contents = params[:atuacao]

    # user.appearance_contents = [params[:escolaridade],
    #                             params[:ascendencia],
    #                             params[:nacionalidade],
    #                             params[:corolho],
    #                             params[:corcabelo],
    #                             params[:idiomas],
    #                             params[:sotaque],
    #                             params[:expressaogenero],
    #                             params[:pele],
    #                             params[:etnia]
    #                           ]

    # user.skill_contents_prof = [params[:circoprofissional],
    #                              params[:dancaprofissional],
    #                              params[:dirigeprofissional],
    #                              params[:esporteprofissional],
    #                              params[:musicaprofissional],
    #                              params[:outrasprofissional]]

    # user.skill_contents_jr = [params[:circoamadoras],
    #                              params[:dancaamadoras],
    #                              params[:dirigeamadoras],
    #                              params[:esporteamadoras],
    #                              params[:musicaamadoras],
    #                              params[:outrasamadoras]]

    i = 0
    while i < params[:cep].count  do
      address = Address.new(:addressLocal => params[:addressLocal][i],
                            :address => params[:address][i],
                            :addressComp => params[:addressComp][i],
                            :zip => params[:cep][i],
                            :addressNeighborhood => params[:addressNeighborhood][i],
                            :addressState => params[:addressState][i],
                            :addressCity => params[:addressCity][i],
                            # :isMainAddress => params[:isMainAddress][i], #este controle nao pode ter o mesmo nome
                            :user_app => user)
      address.save(validate:false)
      i +=1
    end

    if user.save(validate: false)
      ContactMailer.user_subscribe(user).deliver
      
      render json: { :message => 'Usuário criado com sucesso', :user_id => user.id.to_s, :user_name => user.name }, :status => 200
    else
      render json: { :message => 'Não foi possivel criar usuário' }, :status => 500  
    end  
  end
end