class UserApplicant
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip

  field :name, type: String, :default => ""
  field :gender, type: String, :default => "1" #1 - masculino, 2 - feminino
  field :email, type: String, :default => ""
  field :birthday, type: String, :default => ""
  field :parent_name, type: String, :default => ""
  field :parent_cpf, type: String, :default => ""
  field :city, type: String, :default => ""
  field :state, type: String, :default => ""
  field :country, type: String, :default => ""
  field :phone, type: String, :default => ""
  field :desc, type: String, :default => ""
  field :facebook, type: String, :default => ""
  field :instagram, type: String, :default => ""
  field :cacheMin, type: String, :default => ""
  field :seen, type: Boolean, :default => false

  has_many :pictures, :dependent => :destroy
  has_many :videos, :dependent => :destroy
  has_many :answers, :dependent => :destroy
  belongs_to :job
end
