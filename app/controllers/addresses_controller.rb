class AddressesController < ApplicationController
  before_filter :set_address, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @addresses = Address.all
    respond_with(@addresses)
  end

  def show
    respond_with(@address)
  end

  def new
    @address = Address.new
    respond_with(@address)
  end

  def edit
  end

  def create
    @address = Address.new(params[:address])
    flash[:notice] = 'Address was successfully created.' if @address.save
    respond_with(@address)
  end

  def update
    flash[:notice] = 'Address was successfully updated.' if @address.update_attributes(params[:address])
    respond_with(@address)
  end

  def destroy
    @address.destroy
    respond_with(@address)
  end

  private
    def set_address
      @address = Address.find(params[:id])
    end
end
