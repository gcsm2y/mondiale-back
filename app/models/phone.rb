class Phone
  include Mongoid::Document
  field :number, type: String

  belongs_to :user_app
end
