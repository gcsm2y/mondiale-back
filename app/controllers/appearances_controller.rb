class AppearancesController < ApplicationController
  before_filter :set_appearance, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  respond_to :html

  def index
    @appearances = Appearance.all
    respond_with(@appearances)
  end

  def show
    respond_with(@appearance)
  end

  def new
    @appearance = Appearance.new
    respond_with(@appearance)
  end

  def edit
  end

  def create
    @appearance = Appearance.new(params[:appearance])
    flash[:notice_success] = 'Criado com sucesso.' if @appearance.save
    respond_with(@appearance)
  end

  def update
    flash[:notice_success] = 'Atualizado com sucesso.' if @appearance.update_attributes(params[:appearance])
    respond_with(@appearance)
  end

  def destroy
    @appearance.destroy
    respond_with(@appearance)
  end

  private
    def set_appearance
      @appearance = Appearance.find(params[:id])
    end
end
