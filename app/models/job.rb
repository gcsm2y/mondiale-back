class Job
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String
  field :max_age, type: String
  field :min_age, type: String
  field :place, type: String

  field :desc, type: String
  field :jobType, type: String
  field :client, type: String
  field :studio, type: String
  field :product, type: String
  field :placement, type: String
  field :territory, type: String
  field :midia, type: String
  field :exclusivity, type: String
  field :duration, type: String
  field :qtdDaily, type: String
  field :cacheType, type: String
  field :cache, type: String
  field :cacheTotal, type: String
  field :aprovationType, type: String

  field :presential, type: Boolean
  field :aceitaFiguracao, type: Boolean
  field :openVacancy, type: Boolean, :default => true #vaga aberta = true
  field :isPast, type: Boolean, :default => false #Ja terminou o processo seletivo = true
  field :aceitaTesteStatus, type: Boolean, :default => false #Aparecerá na lista de Status para fazer o Teste = true


  field :recording_date, type: String #gravacao
  field :recording_time, type: String #gravacao
  field :recording_place, type: String #gravacao

  # openJob
  field :local, type: String 
  field :perfil, type: String 
  field :rules, type: String 
  field :byWeb, type: Boolean, :default => false # criado como Pesquisa rapida pela Web?

  has_many :userApplicants, :dependent => :destroy

  has_and_belongs_to_many :usersAccept, :class_name => "UserApp", :inverse_of => :jobsAccepted
  has_and_belongs_to_many :usersSubscribed, :class_name => "UserApp", :inverse_of => :jobsSubscribed
  has_and_belongs_to_many :usersProducerAccept, :class_name => "UserApp", :inverse_of => :jobsProducerAccepted

  belongs_to :user_producer
  has_and_belongs_to_many :appearance_contents
  has_and_belongs_to_many :performance_contents

  has_and_belongs_to_many :skill_job_contents_prof, :class_name => "SkillContent", :inverse_of => :job_skill_prof
  has_and_belongs_to_many :skill_job_contents_jr, :class_name => "SkillContent", :inverse_of => :job_skill_jr

  has_many :questions, :dependent => :destroy

  has_many :status_user_jobs

#jobs_controller >openJobs
  def self.mapJob (job, c_user)
    { 
      :id => job.id,
      :title => job.title,
      :description => job.desc,
      :max_age => job.max_age,
      :min_age => job.min_age,
      :isPast => job.isPast,
      :place => job.place,
      :budget => job.cache,
      :duration => job.duration,
      :presential => job.presential,
      :status => Status.mapStatus(job.aceitaTesteStatus == true ? Status.all.asc(:position) : Status.where(:position.ne => 4).asc(:position), c_user.status_user_jobs, job),
      :questions => self.mapQuestions(job.questions, c_user)
    }
  end

  #jobs_controller >openJobs >myJobs
  def self.mapJobs (array, c_user)
    array.map{|job|{ 
      :id => job.id,
      :title => job.title,
      :description => job.desc,
      :max_age => job.max_age,
      :min_age => job.min_age,
      :isPast => job.isPast,
      :place => job.place,
      :budget => job.cache,
      :duration => job.duration,
      :presential => job.presential,
      :status => Status.mapStatus(Status.all.asc(:position), c_user.status_user_jobs, job),
      :questions => self.mapQuestions(job.questions, c_user)
    }}
  end

  def self.mapQuestions (array, c_user)
    array.map{|question|{ 
      :id => question.id,
      :title => question.title,
      :type => question.type,#0 - texto, 1 - link, 2 - imagem,3 - video
      :answer => getAnswer(question.type, question.answers.where(:user_app_id => c_user.id).first)
    }}
  end

  def self.getAnswer (q_type, answer) 
    if !answer.blank?
      if q_type == 2 || q_type == 3
        if !answer.file.blank? && !answer.file.url.include?("missing")
          answer.file.to_s
        end
      else
          answer.content
      end
    end
  end
end
