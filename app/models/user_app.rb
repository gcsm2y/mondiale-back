class UserApp < User
  include Mongoid::Document
  
  field :artisticName, :type => String
  # field :dateLimit, :type => String
  field :responsibleName, :type => String
  field :responsibleCpf, :type => String
  field :addressCity, :type => String, :default => ""
  field :addressState, :type => String, :default => ""
  # field :addressCountry, :type => String, :default => ""
  field :addressDistrict, :type => String, :default => ""
  field :addressComplement, :type => String, :default => ""
  field :addressNumber, :type => String, :default => ""
  field :address, :type => String, :default => ""
  field :issuerUF, :type => String, :default => ""
  field :issuerDate, :type => String, :default => ""
  field :issuer, :type => String, :default => ""

  field :rg, :type => String
  field :rgStartDate, :type => String
  field :orgaoEmissor, :type => String
  field :ufEmissor, :type => String
  field :passportNationality, :type => String
  field :passportExpireDate, :type => String
  field :cnhCategory, :type => String
  field :cnhExpireDate, :type => String

  field :altura, :type => Float, :default => 0
  field :peso, :type => Float, :default => 0
  field :calcado, :type => Float, :default => 0
  field :camisa, :type => Float, :default => 0
  field :terno, :type => Float, :default => 0
  field :camiseta, :type => Float, :default => 0 #1-PP, 2-P, 3-M, 4-G, 5-GG
  field :busto, :type => Float, :default => 0
  field :cintura, :type => Float, :default => 0
  field :quadril, :type => Float, :default => 0
  field :views, type: Integer, :default => 0

  field :aceitaFiguracao, type: Boolean, :default => false
  field :oppositeGenderKiss, type: Boolean, :default => false
  field :sameGenderKiss, type: Boolean, :default => false
  field :nakedSexCover, type: Boolean, :default => false
  field :nakedApparentBreasts, type: Boolean, :default => false
  field :outsideJob, type: Boolean, :default => false

  field :acceptProfile, type: Integer, :default => 0 #0 - Aguardando, 1- Aprovado, 2- Reprovado

  has_mongoid_attached_file :curriculum,
  :storage        => :s3,
  :bucket_name    => 'Mobile2you',
  :bucket    => 'Mobile2you',
  :path           => ':attachment/:id/:style.:extension',
  :s3_credentials => File.join(Rails.root, 'config', 's3.yml')
  
  has_and_belongs_to_many :notifications
  
  belongs_to :schooling
  belongs_to :mannequin_size

  has_and_belongs_to_many :appearance_contents
  # belongs_to :eye_color, :class_name => "AppearanceContent", :inverse_of => :user_eye_color
  # belongs_to :hair_style, :class_name => "AppearanceContent", :inverse_of => :user_hair_style
  # belongs_to :hair_color, :class_name => "AppearanceContent", :inverse_of => :user_hair_color
  # belongs_to :skin, :class_name => "AppearanceContent", :inverse_of => :user_skin
  # belongs_to :ethnicity, :class_name => "AppearanceContent", :inverse_of => :user_ethnicity
  # has_and_belongs_to_many :languages, :class_name => "AppearanceContent", :inverse_of => :user_languages
  # has_and_belongs_to_many :observations, :class_name => "AppearanceContent", :inverse_of => :user_observations
  # has_and_belongs_to_many :gender_expressions, :class_name => "AppearanceContent", :inverse_of => :user_gender_expressions
  # belongs_to :accent, :class_name => "AppearanceContent", :inverse_of => :user_accent
  
  has_and_belongs_to_many :performance_contents
  # has_and_belongs_to_many :jobs

  has_and_belongs_to_many :jobsAccepted, :class_name => "Job", :inverse_of => :usersAccept
  has_and_belongs_to_many :jobsSubscribed, :class_name => "Job", :inverse_of => :usersSubscribed
  has_and_belongs_to_many :jobsProducerAccepted, :class_name => "Job", :inverse_of => :usersProducerAccept

  has_and_belongs_to_many :skill_contents_prof, :class_name => "SkillContent", :inverse_of => :users_prof
  has_and_belongs_to_many :skill_contents_jr, :class_name => "SkillContent", :inverse_of => :users_jr

  
  belongs_to :addressCountry, :class_name => "Nationality", :inverse_of => :user_addressCountry
  belongs_to :nationality, :class_name => "Nationality", :inverse_of => :user_nationalities
  has_and_belongs_to_many :passport_nationalities, :class_name => "Nationality", :inverse_of => :user_passport_nationalities

  has_many :phones, :dependent => :destroy
  has_many :answers, :dependent => :destroy
  has_many :statuses, :dependent => :destroy
  
  has_and_belongs_to_many :cnhs
  belongs_to :origin_country, :dependent => :destroy

  has_many :pictures, :dependent => :destroy
  has_many :videos, :dependent => :destroy
 
  has_many :status_user_jobs

  #account_controller >about
  def self.mapAbout (u)
    { 
      :id => u.id,
      :age => u.birthday.blank? ? "" : age(DateTime.parse(u.birthday)), 
      :place => u.city,
      :flag => u.nationality.nil? ? "" : (u.nationality.file.to_s != "/files/original/missing.png" ? u.nationality.file(:small) : ""),
      :views => u.views
    }
  end

  def self.age(dob)
    now = Time.now.utc.to_date
    years = now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
    if (years == 0)
      months = now.month - dob.month
      if months > 1
        return months.to_s + " meses"
      else
        return months.to_s + " mês"
      end
    else
      if years > 1
        return years.to_s + " anos"
      else
        return years.to_s + " ano"
      end
    end
  end

  #account_controller >myGallery
  def self.mapGallery (c_user)
    gal =[]

    gal = c_user.pictures.map{ |g|{ 
      :id => g.id,
      :picture => g.file.url, 
      :video => nil
    }}

    gal.concat(c_user.videos.map{ |g|{ 
      :id => g.id,
      :picture => nil, 
      :video => g.file.url
    }})

    return gal
  end

#account_controller >getFirstContact
  def self.mapFirstContactNonLogin ()
    { 
      :date_limit => year(18),
      :ocupation => self.mapOcupationsCharacteristicsNonLogin(PerformanceContent.all)
    }
  end

  #account_controller >getFirstContact
  def self.mapFirstContact (u)
    { 
      :id => u.id,
      :full_name => u.name, 
      :artistic_name => u.artisticName, 
      :gender => u.gender,
      :date_limit => year(18),
      :birth_date => u.birthday,
      :responsible_name => u.responsibleName,
      :responsible_cpf => u.responsibleCpf,  
      :picture => (u.facebook.nil? || u.changedPhoto) ?  (u.picture.url == "/pictures/original/missing.png" ? "" : u.picture.url(:original)) : "http://graph.facebook.com/#{u.facebook}/picture?type=large",
      :email => u.email,  
      :phones => mapPhone(u.phones),
      :ocupation => self.mapOcupationsCharacteristics(PerformanceContent.all, u)
    }
  end

  def self.mapPhone (array)
    array.map{|p|{ 
      :id => p.id,
      :phone => p.number
    }}
  end

  def self.year(maximal)
    a = Date.tomorrow
    year = ( a.year - maximal ) - 1
    month = a.mon
    day = a.mday
    Date.new(year,month,day).strftime("%m/%d/%Y")
  end

  #account_controller >getCharacteristics
  def self.mapCharacteristics(ac, c)
    {
      :height_in_cm => c.altura,
      :weight_in_kg => c.peso,
      :shoe_size => c.calcado,
      :shirt_size => c.camisa,
      :suit_size => c.terno,
      :bust_size => c.busto,
      :waist_size => c.cintura,
      :hip_size => c.quadril,
      :eye_color => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Cor dos Olhos"), c),
      :observations => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Observações"), c),
      :hair_style => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Estilo do Cabelo"), c),
      :hair_color => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Cor dos Cabelos"), c),
      :skin => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Peles"), c),
      :ethnicity => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Etnias"), c),
      :languages => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Idiomas"), c),
      :accent => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Sotaques"), c),
      :gender_expressions => self.mapOcupationsCharacteristics(ac.where(:parent_name => "Expressões de Gênero"), c),
      :opposite_gender_kiss => c.oppositeGenderKiss.blank? ? false : c.oppositeGenderKiss, 
      :same_gender_kiss => c.sameGenderKiss.blank? ? false : c.sameGenderKiss, 
      :naked_sex_cover => c.nakedSexCover.blank? ? false : c.nakedSexCover, 
      :naked_apparent_breasts => c.nakedApparentBreasts.blank? ? false : c.nakedApparentBreasts, 
      :extras => c.aceitaFiguracao.blank? ? false : c.aceitaFiguracao, 
      :outside_job => c.outsideJob.blank? ? false : c.outsideJob,
      :mannequin_size => self.mapManequinSize(MannequinSize.all, c)
    }
  end

  def self.mapManequinSize (array, c_user)
    array.map{|manequin|{ 
      :id => manequin.id,
      :name => manequin.size, 
      :isSelected => manequin.user_apps.include?(c_user) ? true : false
    }}
  end

  def self.mapOcupationsCharacteristics(array, c_user)
    array.map { |o| {
      :id => o.id,
      :name => o.name, 
      :isSelected => o.user_apps.include?(c_user) ? true : false
    }}
  end

  def self.mapOcupationsCharacteristicsNonLogin(array)
    array.map { |o| {
      :id => o.id,
      :name => o.name, 
      :isSelected => false
    }}
  end


  #account_controller >personalData
  def self.mapPersonalData (u)
    { 
      :id => u.id,
      :rg => u.rg, 
      :cpf => u.cpf, 
      :issuer_date => u.issuerDate, 
      :issuer => u.issuer, 
      :address_country => self.mapAddressCountry(Nationality.all, u), 
      :address_state => u.addressState, 
      :address_city => u.addressCity, 
      :curriculum => u.curriculum.url == "/curriculums/original/missing.png" ? "" : u.curriculum.url, 
      :address_district => u.addressDistrict, 
      :address_complement => u.addressComplement, 
      :address_number => u.addressNumber, 
      :address => u.address, 
      :issuer_uf => u.issuerUF, 
      :cnh_expiration_date => u.cnhExpireDate, 
      :cnh_category => self.mapCnhCategory(Cnh.all, u),
      :passport_nationality => self.mapPassportNationality(Nationality.all, u),
      :passport_expiration_date => u.passportExpireDate, 
      :schooling => self.mapSchooling(Schooling.all, u),
      :origin => self.mapOrigin(OriginCountry.all, u),
      :nationality => self.mapNationality(Nationality.all, u)
    }
  end

  def self.mapAddressCountry (array, c_user)
    array.map{|country|{ 
      :id => country.id,
      :name => country.name,
      :isSelected => country.user_addressCountry.include?(c_user) ? true : false
    }}
  end

  def self.mapCnhCategory (array, c_user)
    array.map{|cnh|{ 
      :id => cnh.id,
      :name => cnh.name, 
      :isSelected => c_user.cnhs.include?(cnh) ? true : false #cnh.user_apps.include?(c_user) ? true : false
    }}
  end

  def self.mapPassportNationality (array, c_user)
    array.map{|passport|{ 
      :id => passport.id,
      :name => passport.name, 
      :isSelected => c_user.passport_nationalities.include?(passport) ? true : false
    }}
  end

  def self.mapSchooling (array, c_user)
    array.map{|schooling|{ 
      :id => schooling.id,
      :name => schooling.name, 
      :isSelected => schooling.user_apps.include?(c_user) ? true : false
    }}
  end

  def self.mapOrigin (array, c_user)
    array.map{|origin|{ 
      :id => origin.id,
      :name => origin.name, 
      :isSelected => origin.user_apps.include?(c_user) ? true : false
    }}
  end

  def self.mapNationality (array, c_user)
    array.map{|nationality|{ 
      :id => nationality.id,
      :name => nationality.name, 
      :isSelected => c_user.nationality_id == nationality.id ? true : false
    }}
  end


  #account_controller >socialNetworks
  def self.mapSocialNetworks (u)
    { 
      :facebook => u.facebook,
      :instagram => u.instagram, 
      :linkedin => u.linkedin,
      :twitter => u.twitter,
      :skype => u.skype
    }
  end
end
