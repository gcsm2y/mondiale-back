class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Paperclip

  devise :database_authenticatable,:registerable,
         :recoverable, :rememberable, :trackable, :validatable
  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  # registerToken
  field :token, :type => String #"User Token"
  field :os, :type => String #Android 5.1, iOs 11 ...
  field :device, :type => String #MotoG...

  ## Lockable
  field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""
  
  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  # field :facebook, :type => String
  field :uid, :type => String

#Fields
  field :login, :type => String
  field :provider, :type => String
  field :name, :type => String, :default => ""
  field :birthday, :type => String, :default => ""
  field :cpf, :type => String, :default => ""
  field :gender, :type => String, :default => "" #1 - masculino, 2 - feminino
  field :city, :type => String, :default => ""
  field :state, :type => String, :default => ""
  field :country, :type => String, :default => ""

  field :phone, :type => String, :default => ""
  field :cellphone, :type => String, :default => ""
  field :email, :type => String
  field :email2, :type => String
  field :skype, :type => String
  field :website, :type => String

  field :changedPhoto, :type => Boolean, :default => false 

  #Dados Empresa
  field :razaoSocial, :type => String
  field :cnpj, :type => String
  field :inscEstadual, :type => String
  field :inscMunicipal, :type => String
  field :emailCompany, :type => String
  field :phone1Company, :type => String
  field :phone2Company, :type => String
  field :typeCompany, :type => String

  field :facebook, :type => String
  field :instagram, :type => String
  field :linkedin, :type => String
  field :twitter, :type => String
  field :skype, :type => String

  validates_presence_of :name, :message => "digite um nome"
  validates_presence_of :email, :blank => "digite um e-mail"
  validates_confirmation_of :password, :message => "digite uma senha"
  validates :password, confirmation: true
  validates_uniqueness_of :email,:case_sensitive => true, :message => "e-mail ja cadastrado"
  validates_format_of :email, :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i , :message => "e-mail invalido" 

  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'Mobile2you',
    :bucket    => 'Mobile2you',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

  #relationships
  has_many :notifications, :dependent => :destroy

  def isSuperAdmin?
   self.class == SuperAdmin
  end

  def isUserProducer?
   self.class == UserProducer
  end

  def isUserApp?
   self.class == UserApp
  end

#account_controller >getAbout
  def self.mapUser (u)
    { 
      :id => u.id,
      :name => u.name, 
      :email => u.email,
      :birthday => u.birthday,  
      :cellphone => u.cellphone,
      :gender => u.gender,
      :picture => (u.facebook.nil? || u.changedPhoto) ?  (u.picture.url == "/pictures/original/missing.png" ? "" : u.picture.url(:original)) : "http://graph.facebook.com/#{u.facebook}/picture?type=large" 
    }
  end

  #projects_controller
  def self.mapUsers (array)
    array.map{ |u| { 
      :id => u.id,
      :name => u.name, 
      :email => u.email,
      :birthday => u.birthday,  
      :cellphone => u.cellphone,
      :gender => u.gender,
      :picture => (u.facebook.nil? || u.changedPhoto) ?  (u.picture.url == "/pictures/original/missing.png" ? "" : u.picture.url(:original)) : "http://graph.facebook.com/#{u.facebook}/picture?type=large" 
   }}
  end

  #login_controller >signin
  def self.mapUserSignin (u)
    { 
      :id => u.id,
      :name => u.name, 
      :email => u.email,
      :cpf => u.cpf,
      :picture => (u.facebook.nil? || u.changedPhoto) ?  (u.picture.url == "/pictures/original/missing.png" ? "" : u.picture.url(:original)) : "http://graph.facebook.com/#{u.facebook}/picture?type=large" 
    }
  end
end