class MannequinSize
  include Mongoid::Document
  field :size, type: String

  has_many :user_apps
end
