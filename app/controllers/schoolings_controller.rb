class SchoolingsController < ApplicationController
  before_filter :set_schooling, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @schoolings = Schooling.all
    respond_with(@schoolings)
  end

  def show
    respond_with(@schooling)
  end

  def new
    @schooling = Schooling.new
    respond_with(@schooling)
  end

  def edit
  end

  def create
    @schooling = Schooling.new(params[:schooling])
    flash[:notice_success] = 'Schooling was successfully created.' if @schooling.save
    respond_with(@schooling)
  end

  def update
    flash[:notice_success] = 'Schooling was successfully updated.' if @schooling.update_attributes(params[:schooling])
    respond_with(@schooling)
  end

  def destroy
    @schooling.destroy
    respond_with(@schooling)
  end

  private
    def set_schooling
      @schooling = Schooling.find(params[:id])
    end
end
