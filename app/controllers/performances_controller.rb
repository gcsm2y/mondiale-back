class PerformancesController < ApplicationController
  before_filter :set_performance, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  
  respond_to :html

  def index
    @performances = Performance.all
    respond_with(@performances)
  end

  def show
    respond_with(@performance)
  end

  def new
    @performance = Performance.new
    respond_with(@performance)
  end

  def edit
  end

  def create
    @performance = Performance.new(params[:performance])
    flash[:notice_success] = 'Criado com sucesso.' if @performance.save
    respond_with(@performance)
  end

  def update
    flash[:notice_success] = 'Atualizado com sucesso.' if @performance.update_attributes(params[:performance])
    respond_with(@performance)
  end

  def destroy
    @performance.destroy
    respond_with(@performance)
  end

  private
    def set_performance
      @performance = Performance.find(params[:id])
    end
end
