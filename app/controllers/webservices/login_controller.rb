class Webservices::LoginController <  WebservicesController 
  api :POST, '/login/forgotPass'
  formats ['json']
  param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  error 340, "Usuário não encontrado"
  error 500, "Erro desconhecido"
  def forgotPass
    if User.where(:email => params[:email]).count > 0
      render :json => {}
    else
      render :nothing => true, :status => 340
    end
  end


  api :POST, '/login/signin'
  formats ['json']
  param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  param :password, String, :desc => "Password", :required => true, :missing_message => lambda { "senha ausente" }
  error 401, "Usuário não encontrado"
  error 403, "Senha incorreta"
  error 404, "E-mail não cadastrado"
  error 500, "Erro desconhecido"
  description <<-EOS
  == Response
  {
    "id": "0",
    "name": "Jose",
    "email": "jose@mobile2you.com.br",
    "cpf": "00000000000",
    "picture": "https://cdn.modernfarmer.com/wp-content/uploads/2014/12/squirrel.jpg"
  }
  EOS
  def signin
    u = UserApp.where(:email => params[:email]).first
    if !u.nil?
      if u.valid_password?(params[:password]) 
        sign_in u, :bypass => true
        u.save(validate: false)
        render :json => User.mapUserSignin(u)
      else
        render :json => {:message => "Senha incorreta"}, :status => 403
      end
    else
      render :json => {:message => "E-mail não cadastrado"}, :status => 404
    end
  end


  api :POST, '/login/signinFacebook'
  param :name, String, :desc => "Name", :required => true, :missing_message => lambda { "nome ausente" }
  param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  param :facebook, String, :desc => "Facebook", :required => true, :missing_message => lambda { "facebook ausente" }
  error 401, "Usuário não encontrado"
  error 500, "Erro desconhecido"

  def signinFacebook
    u = UserApp.where(:email => params[:email]).first
    if !u.nil?
      u.facebook = (params[:facebook].nil? || params[:facebook].to_s == "empty") ? u.facebook :  params[:facebook]
      sign_in u, :bypass => true
      u.save(validate: false)
      render :json => User.mapUser(u)
    elsif u.nil? && !params[:facebook].nil? 
        u = UserApp.new
        u.password = params[:facebook]
        u.password_confirmation = params[:facebook]
        u.facebook = params[:facebook]
        u.name = params[:name]
        u.email = params[:email]
        u.save(validate: false)
        sign_in u, :bypass => true
        ContactMailer.user_subscribe(u).deliver

        render :json => User.mapUser(u)
    else
      render :nothing => true, :status => 401
    end
  end


  # api :POST, '/login/signup'
  # param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  # param :password, String, :desc => "Password", :required => true, :missing_message => lambda { "senha ausente" }
  # param :cpf, String, :desc => "CPF", :required => false, :missing_message => lambda { "cpf ausente" }
  # param :birthday, String, :desc => "Birthday", :required => false, :missing_message => lambda { "nascimento ausente" }
  # param :cellphone, String, :desc => "Phone", :required => false, :missing_message => lambda { "telefone ausente" }
  # param :gender, String, :desc => "Gender", :required => false, :missing_message => lambda { "sexo ausente" }
  # param :name, String, :desc => "Name", :required => false, :missing_message => lambda { "nome ausente" }

  # error 403, "Usuário já cadastrado"
  # error 500, "Erro desconhecido"

  # def signup
  #   u = UserApp.where(:email => params[:email].downcase).first
  #   if !u.nil?
  #     render :json =>  {:message => "usuário já cadastrado"}, :status => 403
  #   else
  #     user = UserApp.new
  #     user.cpf = params[:cpf]
  #     user.birthday = params[:birthday]
  #     user.cellphone = params[:cellphone]
  #     user.gender = params[:gender]
  #     user.password = params[:password]
  #     user.password_confirmation = params[:password]
  #     user.name = params[:name]
  #     user.picture = params[:profile_picture]

  #     if !params[:picture].nil?
  #       user.picture = params[:picture]
  #     end

  #     user.email = params[:email].downcase
  #     user.save(validate: false)
  #     sign_in user, :bypass => true
  #     render :json =>  User.mapUser(user)
  #   end
  # end

end
