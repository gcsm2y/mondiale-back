class NationalitiesController < ApplicationController
  before_filter :set_nationality, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @nationalities = Nationality.all
    respond_with(@nationalities)
  end

  def show
    respond_with(@nationality)
  end

  def new
    @nationality = Nationality.new
    respond_with(@nationality)
  end

  def edit
  end

  def create
    @nationality = Nationality.new(params[:nationality])
    flash[:notice_success] = 'Nacionalidade criada com Sucesso.' if @nationality.save
    respond_with(@nationality)
  end

  def update
    flash[:notice_success] = 'Nacionalidade atualizada com Sucesso.' if @nationality.update_attributes(params[:nationality])
    respond_with(@nationality)
  end

  def destroy
    @nationality.destroy
    respond_with(@nationality)
  end

  private
    def set_nationality
      @nationality = Nationality.find(params[:id])
    end
end
