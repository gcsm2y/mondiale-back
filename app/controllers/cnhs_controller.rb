class CnhsController < ApplicationController
  before_filter :set_cnh, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @cnhs = Cnh.all
    respond_with(@cnhs)
  end

  def show
    respond_with(@cnh)
  end

  def new
    @cnh = Cnh.new
    respond_with(@cnh)
  end

  def edit
  end

  def create
    @cnh = Cnh.new(params[:cnh])
    flash[:notice_success] = 'Categoria da CNH criada com Sucesso.' if @cnh.save
    respond_with(@cnh)
  end

  def update
    flash[:notice_success] = 'Categoria da CNH atualizada com Sucesso.' if @cnh.update_attributes(params[:cnh])
    respond_with(@cnh)
  end

  def destroy
    @cnh.destroy
    respond_with(@cnh)
  end

  private
    def set_cnh
      @cnh = Cnh.find(params[:id])
    end
end
