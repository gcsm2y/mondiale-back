(function($) {
    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };
}(jQuery));


$(document).ready(function(){

	var menu = $('#headersite .menu');

	var abremenu = function(){
		$('#menu-bars').addClass('active');
		menu.slideDown('fast',function() {
			menu.addClass('active');
			menu.removeAttr('style');
		});
	}

	var fechamenu = function(){
		$('#menu-bars').removeClass('active');
		menu.slideUp('fast',function() {
			menu.removeClass('active');
			menu.removeAttr('style');
		});
	}

	$('#menu-bars').clickToggle(function(){
		abremenu();
	}, function(){
		fechamenu();
	});

	$('#contentWrapper').click(function() {
		if ($('#headersite .menu').hasClass('active')){
			fechamenu();
		};
	})

});

$('#headersite a').click(function(e) {
	e.preventDefault();
	var to = $(this).data('scroll');
	if (to != undefined) {
		$.scrollTo('#'+to || 0, 500);
	}
});

$('.modal').iziModal({
	overlayColor: 'rgba(255, 255, 255, 0.85)',
	radius: 20,
	autoOpen: 0,
	width: 700,
	zindex: 99999
});

//Date Picker Bootstrap
$('.date').datepicker({
    format: "dd/mm/yyyy",
    weekStart: 0,
    todayBtn: false,
    language: "pt-BR",
    multidate: false,
    multidateSeparator: ",",
    keyboardNavigation: false,
    autoclose: true,
    startView: "months",
});

//Date Picker Bootstrap
$('.birthday').datepicker({
    format: "dd/mm/yyyy",
    weekStart: 0,
    todayBtn: false,
    language: "pt-BR",
    multidate: false,
    multidateSeparator: ",",
    keyboardNavigation: false,
    autoclose: true,
    startView: "years",
}).on('change',function(e) {
	var selected = e.target.value;

	if (selected != "") {
		selected = selected.split('/');
		var nasceu = new Date(selected[2],selected[1] - 1,selected[0]);
		var hoje = new Date();
		
		if (hoje > nasceu) {

			if (hoje.getMonth() == nasceu.getMonth()) {

				if (hoje.getDate() >= nasceu.getDate()) {
					var resultado = hoje.getFullYear() - nasceu.getFullYear();
					$('.age').addClass('show').html(resultado + " anos");
				} else {
					var resultado = hoje.getFullYear() - nasceu.getFullYear();
					$('.age').addClass('show').html(resultado - 1 + " anos");
				}

			} else if (hoje.getMonth() > nasceu.getMonth()) {

				var resultado = hoje.getFullYear() - nasceu.getFullYear();
				$('.age').addClass('show').html(resultado + " anos");				
				
			} else if (hoje.getMonth() < nasceu.getMonth()) {
				var resultado = hoje.getFullYear() - nasceu.getFullYear();
				$('.age').addClass('show').html(resultado - 1 + " anos");
				
			}
		} else {
			alert('Selecione uma data de nascimento válida');
			$('.age').removeClass('show');
		}

	} else {
		$('.age').removeClass('show');
	}

});

function validateRequired(required) {
	var n = required.length;
	var c = 0;
	required.forEach(function(v){
		if (v.value != "") {
			c++;
		}
	});
	if (c === n) {
		return true;
	} else {
		return false;
	}
}

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

var SPMaskBehavior = function (val) {
	return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
	onKeyPress: function(val, e, field, options) {
		field.mask(SPMaskBehavior.apply({}, arguments), options);
	}
};

$('input.tel').mask(SPMaskBehavior, spOptions);
$('input.cpf').mask('999.999.999-99');
$('input.cnpj').mask('99.999.999/9999-99');
$('input.cep').mask('99999-999');
$('input.rg').mask('99.999.999-9');
$('input.dataemissao').mask('99/9999');





