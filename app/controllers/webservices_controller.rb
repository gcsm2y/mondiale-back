require 'open-uri'
class WebservicesController <  ActionController::Base 
  before_filter :authenticate_user!, :except => [:createFirstContact, :signin, :signup, :forgotPass, :signinFacebook, :getFirstContact]


  def sendPush(message, to)
    data = { :alert => message, sound:  "default"}
    pushQ = Parse::Push.new(data)
    query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).value_in("user_id", to)
    pushQ.where = query.where
    pushQ.save
  end
 
  def finishOrder (order)
    o = Order.find(order)
    # o.amount = params[:amount].to_f
    c = o.card
    p = Provider.first
    status = ""

    #se a transacao nao tiver iniciado..
    begin
      if o.transaction_id.nil?
        # amount = 100
        transaction = PagarMe::Transaction.new({
            :amount => o.amount,
            # :customer => customer,
            :card => PagarMe::Card.find_by_id(c.token),
            split_rules: [
            { recipient_id: o.store.provider.recipient_id, percentage: (100 - o.service.transfer_percent)},
              {recipient_id: p.recipient_id, percentage: (o.service.transfer_percent)
             }
        ]
        })
        transaction.charge
        o.transaction_id = transaction.id
        status = transaction.status
      else
        transaction = PagarMe::Transaction.find_by_id(o.transaction_id)
        transaction.capture({:amount => o.amount})
        status = transaction.status
      end
      o.status = "Pago"
    rescue
    end
  end

  def scheduler

    orders = Order.where(:status.in => ["Pendente", "Processado"], :date.lte => DateTime.now + 3.hours)
    orders.each do |order|
      if order.status == "Processado"
        finishOrder(order)
        next
      end
      order.status = "Cancelado"
      order.save
      data = {:alert => "Seu pedido foi cancelado por tempo de expiração. Por favor ceja seu calendário", sound:  "default", order: order.id, type: "cancel"}
      if order.side == "Store"
        sendPush(data, "customer_id",order.user.id )
      else 
        sendPush(data, "store_id",order.store.id )
      end
      
      if !order.transaction_id.nil?
        transaction = PagarMe::Transaction.find_by_id(order.transaction_id)
        transaction.refund
      end

    end
    render :json => {}
  end


  def setLatLong
    if !current_user.nil? && !params[:latitude].nil? && !params[:longitude].nil?
      current_user.latitude = params[:latitude].to_f
      current_user.longitude = params[:longitude].to_f
      current_user.save(validate: false)
    end
  end


  def registration
    p = Lead.new
    # p.password = "12345678"
    # p.password_confirmation = "12345678"
    ploomes = Ploomes.first
    uk = ploomes.uk
    
    if ploomes.uk.nil? || ploomes.updated_at.to_date < Date.today
      uk = loginPloomes(ploomes)
    end
    p.name = params[:name]
    p.email = params[:email]
    p.cpf = params[:cpf]
    p.bairro = params[:bairro]
    p.cidade = params[:cidade]
    p.estado = params[:estado]
    p.phone = params[:phone]
    if p.name.length > 0
      p.save
      addCustomer(ploomes,p)
      addOportunity(ploomes,p, params[:especialidade])
    end


    redirect_to "http://www.mobile2you.com.br/loqk"
  end


  def addOportunity(ploomes, p, especialidade)
    json = {}
    json[:Titulo] = especialidade + " - " +  p.cidade
    json[:Cliente] = {:ID_Cliente => p.id_cliente}
    json = json.to_json
    url = "http://www.ploomes.com/"

    response = HTTParty.post("#{url}/api/1/oportunidades?uk=#{ploomes.uk}", :body => json, :headers => { 'Content-Type' => 'application/json' } )
    
    p.id_oportunidade = response.parsed_response.first["ID_Oportunidade"].to_s
    p.save

  end

  def addCustomer(ploomes, p)
    json = {}
    json[:uk] = ploomes.uk
    json[:Cliente] = p.name
    json[:Email] = p.email
    json[:Cidade] = {:Cidade => p.cidade}
    json[:Estado] = p.estado
    json[:Telefones] = []
    json[:Telefones][0] = {:Telefone => p.phone}
    json[:ID_Tipo] = 1
    json = json.to_json
    url = "http://www.ploomes.com/"

    response = HTTParty.post("#{url}/api/1/clientes?uk=#{ploomes.uk}", :body => json, :headers => { 'Content-Type' => 'application/json' } )
    
    p.id_cliente = response.parsed_response.first["ID_Cliente"].to_s
    p.save

  end

  def test
          Loqkmailer.welcome(User.last).deliver
        end

  def loginPloomes (ploomes)
    url = "http://www.ploomes.com/api/1/usuarios/login?email=#{ploomes.email}&senha=#{ploomes.password}"
    doc = JSON.parse(Nokogiri::HTML(open(url)))
    uk = doc.first["Chave"]
    ploomes.uk = uk
    ploomes.save
    uk
  end

  def getRating (store)
    total = 0
    ratings = store.orders.where(:rating_id.ne => nil)
    count = ratings.count
    if ratings.count == 0
      return 1
    else
      ratings.each do |order|
        rating = order.rating
        total += rating.value.to_i
      end
      return (total/count).to_i
    end
  end

  def getCost(store)
    total = 0
    total_store = 0
    prices = store.price_tables
    count = prices.count
    if prices.count == 0
      return 3
    else
      prices.each do |price_table|
        total_store += price_table.price
        total += price_table.service.price
      end

      diff = (total_store - total)/total

      if diff >= 0.5
        return 5
      elsif diff >= 0.25
        return 4
      elsif diff >= 0.25.abs
        return 2
      elsif diff >= 0.5.abs
        return 1
      else
        return 3
      end
    end
  end

end
