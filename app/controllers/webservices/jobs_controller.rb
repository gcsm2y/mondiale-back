class Webservices::JobsController <  WebservicesController
	# api :GET, '/jobs/openPesquisaJobs'
 #   formats ['json']
 #   error 401, "Usuário não autenticado"
 #   def openPesquisaJobs
 #      j = Job.where(:openVacancy => true)
 #      j.each do |a|
 #      if a.title.blank?
 #         a.destroy
 #      end
 #      end
 #      render :json => j
 #   end

	api :GET, '/jobs/openJobs'
	formats ['json']
	error 401, "Usuário não autenticado"
	description <<-EOS
	== Response
	[
	  {
	    "id": "5!5fb56134fc0c8d7c000045",
	    "title": "Campanha publicitaria para marca de sabonete líquido",
	    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi libero augue, malesuada a scelerisque non.",
	    "max_age": 35,
	    "min_age": 18,
	    "place": "Avenida Pedroso de Morais 677, São Paulo, SP",
	    "budget": "R$1500",
	    "duration": "3 horas",
	    "presential": true,
	    "status": [
	      {
	        "id": "lakdfmkdsmf0233mlasimdsa",
	        "title": "Novo Job",
	        "description": "Um novo job para você!",
	        "type": 1,
	        "position": 0,
	        "complete": true
	      },
	      {
	        "id": "lakdfmkdsmf023mlasimdsa",
	        "title": "Em Análise",
	        "description": "Respondemos em até 5 dias úteis",
	        "type": 2,
	        "position": 1,
	        "complete": true
	      }
	    ],
	    "questions": [
	      {
	        "id": "123mlkmfds013m130dsma",
	        "title": "Se necessário, faria cenas de nudez?",
	        "type": 0,
	        "answer": ""
	      },
	      {
	        "id": "123mlkmfds013m130dsma",
	        "title": "Link do facebook",
	        "type": 1,
	        "answer": ""
	      }
	    ]
	  }
	]
	EOS
	def openJobs
		if current_user.acceptProfile == 1
			j = Job.where(:openVacancy => true, :byWeb.ne => true)
			render :json => Job.mapJobs(j, current_user)
		else
			render :json => {:message => "Seu perfil ainda não foi analisado pela Mondiale."}
		end
	end

	api :GET, '/jobs/myJobs'
	formats ['json']
	error 401, "Usuário não autenticado"
	description <<-EOS
	== Response
	[
	  {
	    "id": "5!5fb56134fc0c8d7c000045",
	    "title": "Campanha publicitaria para marca de sabonete líquido",
	    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi libero augue, malesuada a scelerisque non.",
	    "max_age": 35,
	    "min_age": 18,
	    "isPast": true,
	    "place": "Avenida Pedroso de Morais 677, São Paulo, SP",
	    "budget": "R$1500",
	    "duration": "3 horas",
	    "presential": true,
	    "questions": [
	      {
	        "id": "123mlkmfds013m130dsma",
	        "title": "Se necessário, faria cenas de nudez?",
	        "type": 0
	      },
	      {
	        "id": "123mlkmfds013m130dsma",
	        "title": "Link do facebook",
	        "type": 1
	      },
	      {
	        "id": "123mlkmfds013m130dsma",
	        "title": "Foto de rosto",
	        "type": 2
	      },
	      {
	        "id": "123mlkmfds013m130dsma",
	        "title": "Video lavando as mãos",
	        "type": 3
	      }
	    ]
	  },
	  {
	    "id": "5!5fb56134fc0c8d7c000045",
	    "title": "Campanha publicitaria 2",
	    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi libero augue, malesuada a scelerisque non.",
	    "max_age": 35,
	    "min_age": 18,
	    "place": "Avenida Paulista 10000, São Paulo, SP",
	    "budget": "R$3500",
	    "duration": "6 horas",
	    "presential": true,
	    "questions": null
	  }
	]
	EOS
	def myJobs
		j = current_user.jobsSubscribed
		render :json => Job.mapJobs(j, current_user)
	end

	api :POST, '/jobs/apply'
	formats ['json']
  	param :job_id, String, :required => true
	error 401, "Usuário não autenticado"
	error 500, "Erro desconhecido"
	description <<-EOS
	== answers[{question_id, answer}, {String, String}]: array

	== Response
	{
	    "job_id": "1km4l1km4lk1231"
	    "answers": [{ 
	        "question_id": "12312",
	        "answer": "answer"
	    }]
	}
	EOS
	def apply
		if current_user.acceptProfile == 1
			j = Job.find(params[:job_id])
			if !j.usersSubscribed.include?(current_user)
				j.usersSubscribed << current_user
			end

			current_user.status_user_jobs.where(:job_id => j.id).destroy_all
			
			sts = Status.all
			sts.each do |st|
				if st.position == 0
			    	j.status_user_jobs.create(:user_app => current_user, :status_id => st.id.to_s, :status_title => st.title.to_s, :complete => true)
			    else
			    	j.status_user_jobs.create(:user_app => current_user, :status_id => st.id.to_s, :status_title => st.title.to_s, :complete => false)
			    end
			end
			if params[:answers]
				params[:answers].each do |d|
			        q = Question.find(d[:question_id].to_s)
			        if q.type != 2 && q.type != 3
			          	q.answers.create(:user_app => current_user, :content => d[:answer])
		  			end
		      	end
	      	end

			render :json => Job.mapJob(j, current_user)
		else
			render :json => {:message => "Seu perfil ainda não foi analisado pela Mondiale."}
		end
	end

	#Add Media
	api :POST, '/jobs/addMediaAnswer'
	formats ['json']
  	param :question_id, String, :required => true
	error 401, "Usuário não autenticado"
	error 500, "Erro desconhecido"
	description <<-EOS
	== picture :multipart
	== video :multipart
	== Response
	{
	  "question_id": "lkamsdlkasmd"
	}
	EOS
	def addMediaAnswer
		q = Question.find(params[:question_id])
		a = Answer.new
		a.user_app_id = current_user.id
	    a.save(validate:false)

		if !params[:picture].nil?
			a.file = params[:picture]
	    end

	    if (!params[:video].nil?)      
			a.file = params[:video]
      	end

	    a.save(validate:false)
	    q.answers += [a]
	    q.save(validate:false)

		render :json => {}
	end


	#Remove Media
	api :DELETE, '/jobs/removeMediaAnswer'
	formats ['json']
  	param :question_id, String, :required => true
	error 401, "Usuário não autenticado"
	error 500, "Erro desconhecido"
	description <<-EOS
	== Response
	{
	  "question_id": "lkamsdlkasmd"
	}
	EOS
	def removeMediaAnswer
		q = Question.find(params[:question_id])
		a = q.answers.where(:user_app_id => current_user.id)
		a.delete_all

		render :json =>  {}
	end
end