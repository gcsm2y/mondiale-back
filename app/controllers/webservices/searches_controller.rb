class Webservices::SearchesController <  WebservicesController
	
	api :GET, '/searches/getResults'
	formats ['json']
	error 401, "Usuário não autenticado"
	description <<-EOS
	== Response
	[{
	  "id": "59aeaf4e3e85a38b23000050",
	  "question": "Que horas inicia o evento?",
	  "answer": "Ás 08h da manhã"
	}, {
	  
	  "id": "59aeaf4e3e85a38b23000050",
	  "question": "Que horas inicia o evento?",
	  "answer": "Ás 08h da manhã"
	}]
	EOS

	def getResults
		f = Faq.all
		render :json => Faq.mapFaqs(f)
	end

end