class AnswersController < ApplicationController
  before_filter :set_answer, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @answers = Answer.all
    respond_with(@answers)
  end

  def show
    respond_with(@answer)
  end

  def new
    @answer = Answer.new
    respond_with(@answer)
  end

  def edit
  end

  def create
    @answer = Answer.new(params[:answer])
    if @answer.save
      if (!params[:video].nil?)      
        @answer.video = Video.new(file: params[:video])
      end
      flash[:notice_success] = 'Resposta criada com sucesso.'
    end
    respond_with(@answer)
  end

  def update
    if (!params[:video].nil?)
      p = @answer.video
      if p.nil?
        @answer.video = Video.new(file: params[:video])
      else
        p.file = params[:video]
        p.save
      end
    end

    flash[:notice_success] = 'Resposta atualizada com sucesso.' if @answer.update_attributes(params[:answer])
    respond_with(@answer)
  end

  def destroy
    @answer.destroy
    respond_with(@answer)
  end

  private
    def set_answer
      @answer = Answer.find(params[:id])
    end
end
