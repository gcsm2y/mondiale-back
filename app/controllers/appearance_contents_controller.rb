class AppearanceContentsController < ApplicationController
  before_filter :set_appearance_content, only: [:show, :edit, :update, :destroy]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      redirect_to "/admin"
    end
  end
  respond_to :html

  def index
    @appearance_contents = AppearanceContent.all
    respond_with(@appearance_contents)
  end

  def show
    respond_with(@appearance_content)
  end

  def new
    @appearance_content = AppearanceContent.new
    respond_with(@appearance_content)
  end

  def edit
  end

  def create
    @appearance_content = AppearanceContent.new(params[:appearance_content])
    @appearance_content.parent_name = @appearance_content.appearance.name
    flash[:notice_success] = 'Criado com sucesso.' if @appearance_content.save
    respond_with(@appearance_content)
  end

  def update
    @appearance_content.update_attributes(params[:appearance_content])
    @appearance_content.parent_name = @appearance_content.appearance.name
    flash[:notice_success] = 'Atualizado com sucesso.' if @appearance_content.save
    respond_with(@appearance_content)
  end

  def destroy
    @appearance_content.destroy
    respond_with(@appearance_content)
  end

  private
    def set_appearance_content
      @appearance_content = AppearanceContent.find(params[:id])
    end
end
