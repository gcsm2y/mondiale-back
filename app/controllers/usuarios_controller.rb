class UsuariosController < ApplicationController
  # GET /categories
  before_filter :check_admin, except: [:edit, :update]
  before_filter :checkUser

  def checkUser
    if !current_user.isSuperAdmin?
      # render :nothing => true , status: 401
      redirect_to "/admin"
    end
  end
  
  # def index
  #   respond_to do |format|
  #     format.html 
  #     format.json { render json: UsersDatatable.new(view_context, current_user) }
  #   end    
  # end

  # GET /categories/1
  # GET /categories/1.json
  # def show
  #   @user = User.find(params[:id])

  #   respond_to do |format|
  #     format.html # show.html.erb
  #   end
  # end

  def edit
    @user = User.new
  end
  
   # PUT /menus/1.json
  def update
    @usuario = User.find(current_user.id)
    p = params[:user]

    if p[:password].nil?
      if @usuario.update_without_password(p)
        @usuario = User.find(current_user.id)
        redirect_success_home("Perfil com sucesso")
      else
       redirect_error("Por favor, preencha os dados obrigatórios corretamente.", :users, :edit)
      end
    elsif p[:password] == p[:password_confirmation] && p[:password].length >= 8
        if @usuario.update_with_password(p)
          @usuario = User.find(current_user.id)
          sign_in @usuario, :bypass => true
          redirect_to("/alterar_senha", :flash => { :notice_success => "Senha alterada com sucesso." })   
        else
          redirect_to("/alterar_senha", :flash => { :notice_error => "Insira a senha atual corretamente." })   
        end
    else
      redirect_to("/alterar_senha", :flash => { :notice_error => "A nova senha deve ter 8 ou mais caracteres." })   
    end
  end


  # DELETE /categories/1
  # DELETE /categories/1.json
  # def destroy
  #   @category = User.find(params[:id])
  #   Chat.where(:user_1 => params[:id]).destroy_all
  #   Chat.where(:user_2 => params[:id]).destroy_all
  #   @category.destroy

  #   respond_to do |format|
  #     format.html { redirect_success("Usuário removido com sucesso!",:users, :index)}
  #     format.json { head :no_content }
  #   end
  # end

  def check_admin
    if !current_user.isSuperAdmin?
      render_404
    end
  end
end
