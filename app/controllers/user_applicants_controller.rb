class UserApplicantsController < ApplicationController
  before_filter :set_user_applicant, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    # @user_applicants = UserApplicant.all
    if current_user.class == SuperAdmin
      @jobs = Job.where(:byWeb => true)
    else
      @jobs = Job.where(:byWeb => true, :user_producer_id => current_user.id)
    end

    respond_to do|format|
      format.html
      format.json
      format.pdf do
        pdf = ReportPdf.new(@jobs)
        send_data pdf.render, filename: 'report.pdf', type: 'application/pdf'
      end
       # {render template: 'user_applicants/reporte', pdf: 'Reporte'}
    end
    # respond_with(@jobs)
  end

  def show
    # respond_to do|format|
    #   format.html
    #   format.json
    #   format.pdf {render template: 'user_applicants/reporte', pdf: 'Reporte'}
    # end
    respond_with(@job)
  end
  

  def new
    # @user_applicant = UserApplicant.new
    # respond_with(@user_applicant)
  end

  def edit
  end

  def create
    # @user_applicant = UserApplicant.new(params[:user_applicant])
    # flash[:notice] = 'UserApplicant was successfully created.' if @user_applicant.save
    # respond_with(@user_applicant)
  end

  def update
    flash[:notice] = 'Pesquisa atualizada com sucesso.' if @job.update_attributes(params[:job])
    respond_with(@job)
  end

  def send_email
    if !params[:chk_user].blank?
      users = UserApplicant.where(:id.in => params[:chk_user])
      if users.count > 0
        jodId = users[0].job.id
        prodEmail = users[0].job.user_producer.email
        ContactMailer.user_link(prodEmail.to_s, users).deliver
        redirect_to "/user_applicants/" + jodId
      else
        redirect_to "/user_applicants"
      end

    end
  end

  def seen
    user = UserApplicant.find(params[:id])
    if user.seen
      user.seen = false
    else
      user.seen = true
    end
    user.save
    render :json => {:seen => user.seen}
    # redirect_to "/user_applicants/" + user.job.id
  end

  def destroy_many
    if !params[:chk_user].blank?
      users = UserApplicant.where(:id.in => params[:chk_user])
      jodId = users[0].job.id
      users.destroy_all
      redirect_to "/user_applicants/" + jodId
    end
  end

  def destroy
    if @job.blank?
      @user.destroy
      redirect_to "/user_applicants/" + @user.job.id
    else
      @job.destroy
    redirect_to "/user_applicants"
    end
  end

  private
    def set_user_applicant
      @job = Job.where(:id => params[:id]).first
      if @job.blank?
        @user = UserApplicant.where(:id => params[:id]).first
      end
    end
end
