class Nationality
  include Mongoid::Document
  include Mongoid::Paperclip  

  field :name, type: String

  has_mongoid_attached_file :file,
      :storage        => :s3,
      :bucket_name    => 'Mobile2you',
      :bucket    => 'Mobile2you',
      :styles => { :small => "20x20>" },
      :path           => ':attachment/:id/:style.:extension',
      :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

  has_many :user_nationalities, :class_name => "UserApp", :inverse_of => :nationality
  has_many :user_addressCountry, :class_name => "UserApp", :inverse_of => :addressCountry
  has_and_belongs_to_many :user_passport_nationalities, :class_name => "UserApp", :inverse_of => :passport_nationalities
end
