class Question
  include Mongoid::Document
  field :title, type: String
  field :type, type: Integer, :default => 0 #0 - texto, 1 - link, 2 - imagem,3 - video

  belongs_to :job
  has_many :answers, :dependent => :destroy
end
