class Notification
include Mongoid::Document
  include Mongoid::Timestamps::Created
  field :message, type: String
  field :date, type: DateTime
  field :sent, type: Boolean, :default => false
  field :sendNow, type: Boolean, :default => false

  has_and_belongs_to_many :user_apps

  before_save :checkPush

  def checkPush
    if self.sendNow
        if !self.sent && (self.date.nil? || Time.now >= self.date)
          self.sent = true
          self.date = Time.now
          registration_ids = self.user_apps.distinct(:token)
          if registration_ids.count > 0
            fcm = FCM.new("AAAAujxSqNw:APA91bFlLHynldrznqIFKJw8o-10JLJGragcBPN2-sWrsnISDoy-KyuPUDLThO2ei7gNVRl8fidTjwcXdDtHiRYctCxPQOgWf1gTUp0gxePiMMCmSk8lhOA0dKKqa4aH2bUPSScPRMFd")
            options_ios = {}
            options_ios[:notification] = {}
            options_ios[:notification][:title] = "Mondiale Casting"
            options_ios[:notification][:body] = self.message
            options_ios[:notification][:color] = 'red'
            options_ios[:content_available] = true
            options_ios[:data] = {}
            options_ios[:data][:priority] = 'high'
            options_ios[:data][:message] = self.message
            response = fcm.send(registration_ids, options_ios)
          end
        end
    end
  end


#account_controller >notifications
  def self.mapNotifications (array)
    array.map { |n| {
      :id => n.id,
      :description => n.desc, 
      :time => n.created_at.strftime("%d/%m/%Y %H:%M") 
   	}}
  end
end
