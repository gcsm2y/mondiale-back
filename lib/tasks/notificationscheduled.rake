desc "Send push rake - Mensagens e datas criadas pelo Administrador"
task notificationscheduled: :environment do
  Notification.where(:date.lte => Time.now , :sent => false).each do |n|
    n.sendNow = true
    n.sent = true
    n.save(validate:false)
  end
end