class PassportNationalitiesController < ApplicationController
  before_filter :set_passport_nationality, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @passport_nationalities = PassportNationality.all
    respond_with(@passport_nationalities)
  end

  def show
    respond_with(@passport_nationality)
  end

  def new
    @passport_nationality = PassportNationality.new
    respond_with(@passport_nationality)
  end

  def edit
  end

  def create
    @passport_nationality = PassportNationality.new(params[:passport_nationality])
    flash[:notice_success] = 'PassportNationality was successfully created.' if @passport_nationality.save
    respond_with(@passport_nationality)
  end

  def update
    flash[:notice_success] = 'PassportNationality was successfully updated.' if @passport_nationality.update_attributes(params[:passport_nationality])
    respond_with(@passport_nationality)
  end

  def destroy
    @passport_nationality.destroy
    respond_with(@passport_nationality)
  end

  private
    def set_passport_nationality
      @passport_nationality = PassportNationality.find(params[:id])
    end
end
