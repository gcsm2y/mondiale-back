Apipie.configure do |config|
  config.app_name                = "Documentação"
  config.api_base_url            = "/webservices"
  config.doc_base_url            = "/apidocs"
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
end
