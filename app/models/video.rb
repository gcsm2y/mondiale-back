class Video
  include Mongoid::Document
  include Mongoid::Paperclip  

  has_mongoid_attached_file :file,
      :storage        => :s3,
      :bucket_name    => 'Mobile2you',
      :bucket    => 'Mobile2you',
      :path           => ':attachment/:id/:style.:extension',
      :s3_credentials => File.join(Rails.root, 'config', 's3.yml'),
      :source_file_options => { all:     '-auto-orient' }

  belongs_to :user_app
  belongs_to :user_applicant
end