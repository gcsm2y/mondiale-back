class OriginCountriesController < ApplicationController
  before_filter :set_origin_country, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @origin_countries = OriginCountry.all
    respond_with(@origin_countries)
  end

  def show
    respond_with(@origin_country)
  end

  def new
    @origin_country = OriginCountry.new
    respond_with(@origin_country)
  end

  def edit
  end

  def create
    @origin_country = OriginCountry.new(params[:origin_country])
    flash[:notice_success] = 'Ascendência criada com sucesso.' if @origin_country.save
    respond_with(@origin_country)
  end

  def update
    flash[:notice_success] = 'Ascendência atualizada com sucesso.' if @origin_country.update_attributes(params[:origin_country])
    respond_with(@origin_country)
  end

  def destroy
    @origin_country.destroy
    respond_with(@origin_country)
  end

  private
    def set_origin_country
      @origin_country = OriginCountry.find(params[:id])
    end
end
